#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

cd "$(dirname "${BASH_SOURCE[0]}")/.."

# Generate truncated version rules from Jsonnet
jsonnet-tool render \
  -m . \
  src/truncated-versions/truncated-versions.jsonnet
