# [`editorconfig-check`](./editorconfig-check.yml)

This job ensures that the repository adheres to the [EditorConfig](https://editorconfig.org/) rules for the project.

- Stages: `validate`
- Expects config file: `.editorconfig`

```yaml
stages:
  - validate

include:
  # validate .editorconfig
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/editorconfig-check.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: "editorconfig-check.yml"
```

## How do address CI validation failures from the `editorconfig-check` job?

The `editorconfig_check` job attempts to validate against basic whitespace errors. The rules are set in the industry-standard `.editorconfig` file, located in the root of the project.

Many popular source-code editors will automatically use the rules configured in the `.editorconfig` file: see <https://editorconfig.org/#pre-installed> for the list. Alternatively, install a plugin using the instructions at <https://editorconfig.org/#download>.

Once your editor is able to parse the `.editorconfig` file, if should automatically be configured with the appropriate settings on a per-file basis.

### Manually checking your configuration locally

In some cases, you may want to manually validate the editorconfig settings in a repository. This can easily be done by installing the `editorconfig-checker` tool and running it, like so:

```shell
go install github.com/editorconfig-checker/editorconfig-checker/cmd/editorconfig-checker@latest
editorconfig-checker
```

If your `$GOPATH` is not in your `$PATH`, you can use something like this to invoke `editorconfig-checker`:

```shell
$(go env GOPATH)/bin/editorconfig-checker
```

Run `go env GOPATH` to see the full path to `GOPATH`.
