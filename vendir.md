
# [`vendir`](./vendir.yml)

Runs [`vendir sync`](https://github.com/carvel-dev/vendir) and validates that the committed state matches the
output from the command.


```yaml
stages:
  - validate

variables:
  # Override the vendir config file, default is vendir.yml
  VENDIR_CONF: vendir.yml

include:
  # Check that `vendir sync` is up-to-date and the repository matches the expected state
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: vendir.yml
```

## Using a GitLab CI Token in `vendir`

This task will run `vendir` and pre-configure a secret called `gitlab-token-http-auth`, containing the `CI_JOB_TOKEN` for authentication.

This is a `vendir` HTTP authentication secret. It can be used with a `secretRef`, as follows:

```yaml
apiVersion: vendir.k14s.io/v1alpha1
kind: Config
directories:
- path: vendor
  contents:
    - path: ...
      http:
        url: https://gitlab.com/api/v4/...
      secretRef:
        name: gitlab-token-http-auth
```
