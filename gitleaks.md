# [`gitleaks`](./gitleaks.yml)

Last ditch effort to detect whether secrets have accidentally been checked into Git.

This CI job checks for secrets using [`gitleaks`](https://github.com/zricethezav/gitleaks). Note that by the time CI is running, _it is already too late_ - the secrets have already been pushed to CI.

This is why we consider this job to be a last ditch effort.

Preferably, `gitleaks` should be executed before commit. If you're using the [`common-template-copier`](https://gitlab.com/gitlab-com/gl-infra/common-template-copier) template, a `pre-commit` job should be pre-configured in the project [`.pre-commit-config.yaml`](https://gitlab.com/gitlab-com/gl-infra/common-template-copier/-/blob/main/.pre-commit-config.yaml.jinja) file.

As an alternative, consider using the GitLab [`gitleaks-endpoint-installer`](https://gitlab.com/gitlab-com/gl-security/security-research/gitleaks-endpoint-installer), but this will install a global hook which may interfere with other hooks.

**Note**: By default, `gitleaks` will check all commits in a repository. This CI job will only check the current change, not all commits. The default behavior will lead to all MRs failing if any MR contains a leak.

* **Step 1:** add a `.gitleaks.toml` file to your repository:

```toml
title = "project-scoped gitleaks config"

[extend]
# useDefault will extend the base configuration with the default gitleaks config:
# https://github.com/zricethezav/gitleaks/blob/master/config/gitleaks.toml
useDefault = true

# See https://github.com/zricethezav/gitleaks#configuration
# for details of extending this configuration for allowlists,
# etc
```

* **Step 2:** include the `gitleaks.yml` task.

```yaml
stages:
  - validate

include:
  # Ensure that all shell-scripts are formatted according to a
  # standard canonical format
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: gitleaks.yml
```
