# [`hclfmt`](./hclfmt.yml)

Runs [`hclfmt`](https://github.com/hashicorp/hcl/tree/main/cmd/hclfmt) to ensure that all HCL files are valid and correctly formatted.

```yaml
stages:
  - validate

include:
  # Ensures that all terraform files are correctly formatted
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/hclfmt.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: hclfmt.yml
```

## Running `hclfmt` locally

To run `hclfmt`, locally, install it using `go install`:

```shell
go install github.com/hashicorp/hcl/v2/cmd/hclfmt@latest
```

Then run `hclfmt` passing it all the `.hcl` files as arguments:

```shell
find . -name '*.hcl'|xargs hclfmt -w -require-no-change
```

## Pre-Commit Hook

To use `hclfmt` with [`pre-commit`](https://pre-commit.com/), use the following hook configuration:

```yaml
  - repo: local
    hooks:
      - id: hclfmt
        name: hclfmt
        language: golang
        entry: hclfmt -w -require-no-change
        additional_dependencies:
          - github.com/hashicorp/hcl/v2/cmd/hclfmt@latest
        files: '^(.*\.hcl)$'
```
