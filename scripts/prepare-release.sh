#!/usr/bin/env bash

# This script updates all self version references in
# the common-ci-tasks project in preparation for the next release

set -euo pipefail
IFS=$'\n\t'

next_version=$1

inline_sed() {
  if command -v gsed >/dev/null; then
    gsed -i "$@"
  elif [[ $OSTYPE == 'darwin'* ]]; then
    sed -i '' "$@"
  else
    sed -i "$@"
  fi
}

for i in *.md templates/*.md *.yml templates/*.yml docs/*.md; do
  inline_sed -E 's/(re[fv]): v[[:digit:]]+\.[[:digit:]]+(\.[[:digit:]]+)? +# *renovate:managed/\1: v'"$next_version"'  # renovate:managed/' "$i"

  inline_sed -E 's#(registry\.gitlab\.com|\$\{?CI_REGISTRY\}?)/gitlab-com/gl-infra/common-ci-tasks/([[:alpha:]]+):v[[:digit:]]+\.[[:digit:]]+(\.[[:digit:]]+)?#\1/gitlab-com/gl-infra/common-ci-tasks/\2:v'"$next_version"'#' "$i"
done

# Update pre-commit config
inline_sed -E 's/rev: v[[:digit:]]+\.[[:digit:]]+(\.[[:digit:]]+)? +# *renovate:managed:self/rev: v'"$next_version"'  # renovate:managed:self/' ".pre-commit-config.yaml"
