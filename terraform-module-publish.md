# [`terraform-module-publish`](./terraform-module-publish.yml)

The purpose of this project is to establish a standardized way to publish a
Terraform Module to the GitLab Terraform Module Registry. Your first step is to
add the following code block to your repository's CI task. In most cases this
means adding the following project to the `include` section of the
.gitlab-ci.yml file at the root of your repository.

```yaml
include:
  # Publish a Terraform Module in the Terraform Module registry
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/terraform-module-publish.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: terraform-module-publish.yml
    inputs:
      module_name: # Defaults to project name
      module_system: # aws,google,etc
```

## Excluding files from the Terraform Module

To exclude files from the tarball, configure a `.tfmoduleignore` file in the root of the project.

This will reduce the size of the module,
improving performance throughout the development and provisioning cycle.

Dot files, including `.git`, will also be excluded by default.

### Example `.tfmoduleignore`

This snippet demonstrates the format of `.tfmoduleignore`:

```console
$ cat .tfmoduleignore
CODEOWNERS
*.md
renovate.json
scripts/**
```
