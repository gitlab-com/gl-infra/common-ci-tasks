# [`shellcheck`](./shellcheck.yml)

Performs linting for shell scripts using [Shellcheck](https://www.shellcheck.net/).

1. Ensure that the `GL_ASDF_SHELLCHECK_VERSION` version is configured.
1. Supports the `COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP` variable, for excluding paths from validation.
1. Arguments for Shellcheck can optionally be passed through the variable `SHELLCHECK_ARGS`

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_SHELLCHECK_VERSION: ...
  # Exclude vendor and files directories from validation
  COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP: '^\.(/vendor/|/files/)'
  # Any arguments for shellcheck
  SHELLCHECK_ARGS: --source-path=. --source-path=./test/ --source-path=./test/lib/ --source-path=./test/integration/ --external-sources

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Ensure that all shell-scripts are formatted according to a
  # standard canonical format
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/shellcheck.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: shellcheck.yml
```
