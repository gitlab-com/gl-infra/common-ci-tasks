#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

main() {
  # Update .gitlab-ci-asdf-versions.yml
  if [[ -x ./scripts/update-asdf-version-variables.sh ]]; then
    ./scripts/update-asdf-version-variables.sh
  fi
}

main
