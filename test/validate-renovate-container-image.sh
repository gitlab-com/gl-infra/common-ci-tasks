#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

yq --version
jb --version
go version
yarn -v
ruby --version
mise --version

# Check that the post update scripts work...
/opt/gitlab/renovate/renovate-upgrade-scripts/shfmt.sh

# Check that renovate-config-validator is available
npx renovate-config-validator
