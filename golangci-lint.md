
# [`golangci-lint`](./golangci-lint.yml)

Runs [golangci-lint](https://github.com/golangci/golangci-lint) on the project.

* Stages: `validate`

```yaml
stages:
  - validate

include:
  # Runs golangci-lint on the project.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/golangci-lint.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'golangci-lint.yml'
    inputs:
      golangci_lint_timeout: 5m # optionally specify a separate timeout
```

## Configuration

This template receives the following input variables:

- `stage`: The stage in which the `golangci_lint` job runs
  Default: `validate`
- `docker_hub_host`: The docker hub host from which to pull the
  `golangci-lint` image.
  Default: `docker.io`
- `golangci_lint_timeout`: The timeout used for `golangci-lint`
  Default: `5m`
