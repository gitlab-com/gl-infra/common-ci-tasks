module gitlab.com/gitlab-com/gl-infra/common-ci-tasks

go 1.24.1

// Note: this file is only here to allow golang pre-commit hooks to be installed via this repository
