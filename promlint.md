# [`promlint`](./promlint.yml)

Validates that Prometheus Alerts, Rules, and Queries are correctly defined.

1. Ensure that the `GL_ASDF_PINT_VERSION` version for [Pint](https://github.com/cloudflare/pint) is configured.
1. Define a `.pint.hcl` in the root of the target project, setting at a minimum the `baseBranch` and `include` paths to check for changes.

* By default `pint ci` is only checking Prometheus changes that aren't on the baseBranch.
  * To manually validate the entirety of the rules run:
    `pint lint --offline /path/to/rule.yml`
```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_PINT_VERSION: ...

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Ensure that all Prometheus Rules and Alerts are defined correctly
  # Rules defined https://cloudflare.github.io/pint/checks/
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: promlint.yml
```

Example HCL
```hcl
ci {
  baseBranch = "main"
  include = [
    "metrics-catalog/get-hybrid/config/prometheus-rules/.*",
    "prom-rules/.*"
  ]
}
```
