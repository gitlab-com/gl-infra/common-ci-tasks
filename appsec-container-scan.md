# Appsec Container Scanner

## [`.appsec_container_scan`](./appsec_container_scan.yml)

This task can be used to trigger a pipeline for scanning container images and include the container scanning results in the project that is triggering this scan.
The pipeline is triggered in the [`container-scanners`](https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/) project which is used, maintained by the GitLab Appsec team.

`CONTAINER_SCAN_PIPELINE_TRIGGER_TOKEN` and `CONTAINER_SCAN_PROJECT_API_TOKEN` CI/CD variables need to be configured on the project that is triggering this scan.
See [this section](https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/tree/master/#how-to-trigger-a-new-container-scan-and-include-results-in-the-target-project) for more information.
These variables should already be available to projects under `gl-infra` subgroup as they were added as Group CI/CD variables.

```yaml
include:
  # This task can be used to trigger a pipeline for scanning container images
  # and include the container scanning results in the project that is triggering this scan.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/appsec-container-scan.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: appsec-container-scan.yml

container_image_scan:
  stage: validate
  variables:
    IMAGES: "Set this to be the names of the images you wish to scan, comma delimited"
  needs:
    - container_image_build
  extends:
    - .appsec_container_scan
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
```
