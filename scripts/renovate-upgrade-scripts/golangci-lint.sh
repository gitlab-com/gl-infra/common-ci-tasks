#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

main() {
  mise install golangci-lint
  eval "$(mise activate bash --shims)"

  echo "Reapplying golangci/golangci-lint with --fix to fix any new issues..."

  for to_disable in mnd perfsprint; do
    if (find_linter "^${to_disable}\\b"); then
      echo "$to_disable found, disabling"

      # Add to linters.disable
      yq eval --inplace '.linters.disable=(.linters.disable + ["'"${to_disable}"'"] | unique | sort)' .golangci.yaml
    else
      echo "$to_disable not found, ignoring"
    fi
  done

  for to_remove in scopelint exhaustivestruct golint interfacer maligned ifshort; do
    if find_linter "^${to_remove} \\[deprecated\\]" || ! (find_linter "^${to_remove}\\b"); then
      # Remove from linters.disable
      yq eval --inplace '.linters.disable=[.linters.disable|.[] | select(. != "'"${to_remove}"'")]' .golangci.yaml
    fi
  done

  # yq doesn't preserve whitespace, so we use the patch hack to preserve whitespace
  # https://github.com/mikefarah/yq/issues/515#issuecomment-830380295
  git diff -U0 -w -b --ignore-blank-lines .golangci.yaml >.golangci.yaml.patch
  git checkout -- .golangci.yaml
  patch .golangci.yaml <.golangci.yaml.patch
  rm .golangci.yaml.patch

  golangci-lint run --fix || true # Don't treat a failed exit code as a script failure
}

find_linter() {
  # shellcheck disable=SC2143
  # For some reason grep -q isn't working here
  if [[ -z $(golangci-lint help linters --color never | grep "$1") ]]; then
    return 1
  fi
}

main
