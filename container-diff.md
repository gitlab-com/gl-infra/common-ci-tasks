# [`container-diff`](./container-diff.yml)

This job produces output to compare the size of a container image before and after an merge-request change.

This can help to determine how much a container image has changed in size due to the current merge request.

```yaml
include:
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'container-diff.yml'
    inputs:
      job_name: container-diff # The name of the job this template will create
      compare_from_image: $CI_REGISTRY_IMAGE/base:main # The base image for the comparison
      compare_to_image: $CI_REGISTRY_IMAGE/base:${CI_COMMIT_REF_SLUG} # The image created by the build job
      needs: kaniko build # The job that will produce the images to compare
      stage: docker_build # The stage this job should run in
```

This will generate an output along the following lines:

```terraform
---------------------------------------------------------------------
A: alpine:3.19 is   3.26MiB
B: alpine:latest is   3.46MiB
---------------------------------------------------------------------
Difference (B - A) is 201.68KiB
```

This information can be used by reviewers to evaluate the impact of a change on the size of the container image produced.
