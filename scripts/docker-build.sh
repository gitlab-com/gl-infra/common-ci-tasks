#!/usr/bin/env bash

#######################################
# This program allows for docker builds
# to be run easily, locally, with the correct
# --build-args supplied
#
# This can be helpful during development.
#
# ./scripts/docker-build.sh <docker build arguments>
#######################################

set -euo pipefail
IFS=$'\n\t'

main() {
  # shellcheck disable=SC2046
  docker build $(generate_build_args) "$@"
}

generate_build_args() {
  yq '.variables | to_entries|.[]|"--build-arg=" + .key + "=" + .value' \
    .gitlab-ci-asdf-versions.yml .gitlab-ci-default-asdf-versions.yml .gitlab-ci-other-versions.yml
}

main "$@"
