# [`templates/golang.yml`](./templates/golang.yml)

This template should be used for Golang projects.

Includes the following tasks:

1. [`go-mod-tidy.yml`](../go-mod-tidy.md): ensures that [`go mod tidy`](https://go.dev/ref/mod) is up-to-date and `go.mod` and `go.sum` are tidy.
1. [`go-unittests.yml`](../go-unittests.md): runs unit tests to ensure and emits a JUnit XML report for GitLab.
1. [`golangci-lint.yml`](../golangci-lint.md): runs [golangci-lint](https://github.com/golangci/golangci-lint) on the project.
1. [`goreleaser.yml`](../goreleaser.md): builds a binary release of the project using [GoReleaser](https://goreleaser.com/).

```yaml
# Requires stages validate and release
stages:
  - validate
  - release

# Better to define these through .gitlab-ci-asdf-versions.yml
variables:
  GL_ASDF_GOLANG_VERSION: ...
  GL_ASDF_GOLANGCI_LINT_VERSION: ...
  GL_ASDF_GORELEASER_VERSION: ...

include:
  - local: .gitlab-ci-asdf-versions.yml
  # Runs golang standard tests, including tests, goreleaser, golangci-lint and go-mod-tidy
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/templates/golang.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: templates/golang.yml
```

## Overriding Stages

You can override the stages that a CI/CD job runs in [using inputs](https://docs.gitlab.com/ee/ci/yaml/includes.html#define-input-parameters-with-specinputs).

```yaml
include:
  - local: .gitlab-ci-asdf-versions.yml
  # This template should be included in all Infrastructure projects.
  # It includes standard checks, gitlab-scanners, validations and release processes
  # common to all projects using this template library.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/templates/standard.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: templates/golang.yml
    inputs:
      validate_stage: test  # stage that validation jobs should run in
      release_stage: deploy  # stage that release jobs should run in
```

## Other configuration

The following values can also be configured and are passed to the
relevant tasks:

- `golangci_lint_timeout`: [`golangci-lint.yml`](../golangci-lint.md)
