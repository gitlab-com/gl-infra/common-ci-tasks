#!/usr/bin/env bash

#
# Run tflint
#

set -euo pipefail
IFS=$'\n\t'

eval "$(mise activate bash --shims)"

# Note: pre-commit will always run in the root directory of the project
root_dir=${PWD}

main() {
  tflint --init --config "${root_dir}/.tflint.hcl"

  local tflint_failed=false
  while read -r module; do
    # We use --call-module-type=none since each module is validated independently for performance reasons
    tflint --config "${root_dir}/.tflint.hcl" --chdir="${module}" --call-module-type=none --fix || {
      tflint_failed=true
    }
  done < <(list_modules "$@")

  if [[ ${tflint_failed} == true ]]; then
    exit 1
  fi
}

list_modules() {
  for i in "$@"; do
    dirname "$i"
  done | sort -u
}

main "$@"
