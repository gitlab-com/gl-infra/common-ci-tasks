# [`go-unittests`](./go-unittests.yml)

Runs unit tests to ensure and emits a JUnit XML report for GitLab.

1. Ensure that the `GL_ASDF_GOLANG_VERSION` version for Go is configured.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_GOLANG_VERSION: ...
  GO_UNITTESTS_EXCLUDE_PACKAGES_REGEXP: ....

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Runs Go unit tests
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/go-unittests.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: go-unittests.yml
```

## Skipping Integration Tests

Integration tests may require additional setup and may not be suitable for running with a basic unit test runner.

To exclude them, use the `GO_UNITTESTS_EXCLUDE_PACKAGES_REGEXP` variable to match any packages to exclude from the go unit tests.

```yaml
variables
GO_UNITTESTS_EXCLUDE_PACKAGES_REGEXP: 'gitlab.com/gl-infra/blah/cmd'
```
