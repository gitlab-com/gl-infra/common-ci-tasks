# [`yamllint`](./yamllint.yml)

Runs [`yamllint`](https://yamllint.readthedocs.io/) across the repository.

Setup process:

1. Ensure that a [`.yamllint.yaml`](https://yamllint.readthedocs.io/en/stable/configuration.html#extending-the-default-configuration) file exists in the root of the project.

As a starting point, consider using this configuration file:

```yaml
# .yamllint.yaml
extends: default
```

1. Add the yamllint job via an include. Note that this will automatically be included via [`standard.yml`](./templates/standard.md).

```yaml
stages:
  - validate

include:
  # Runs yamllint on all terraform module directories
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/yamllint.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: yamllint.yml
```
