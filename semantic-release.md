
# [`semantic-release`](./semantic-release.yml)

This job will run [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/) on your repository. This allows the repository to be automatically tagged based on [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) messages.

If the `.releaserc.json` file is changed, the merge request will validate the change in the `semantic_release_check` job.

Further Reading:

1. [Automate Semantic Versioning with Conventional Commits](https://medium.com/@jsilvax/automate-semantic-versioning-with-conventional-commits-d76a9f45f2fa).
1. [Basic Guide to Semantic Release](https://levelup.gitconnected.com/basic-guide-to-semantic-release-9e2aa7834e4b)

## Setup guide

### Happy path

1.  Manage the GitLab project in [gl-infra/infra-mgmt](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/):

    ```hcl
    module "project_example" {
      source  = "ops.gitlab.net/gitlab-com/project/gitlab"
      version = "3.0.1" # renovate:managed

      common_ci_tasks = {
        enabled = true
      }

      vault = {
        enabled   = true
        # …
      }

      # …
    }
    ```

    This automatically creates a [Project Access Token](#project-access-token) for Semantic Release and stores it in Vault.
1.  Use `copier` to initialize the repository:

    ```shell
    copier copy https://gitlab.com/gitlab-com/gl-infra/common-template-copier.git new-repo/
    ```

    This creates a `.gitlab-ci.yml` file that includes `semantic-release.yml` via `templates/standard.yml`.

Done :tada:

### Manual setup

1.  Create a [Project Access Token](#project-access-token) for Semantic Release.
1.  Store the *Project Access Token* in [Vault](#vault) or a [CI variable](#ci-variable).
1.  Create a `.releaserc.json` file in your project. Copy this one if you are unsure.

    ```json
    {
      "branches": ["main"],
      "plugins": [
        [
          "@semantic-release/commit-analyzer",
          {
            "preset": "conventionalcommits"
          }
        ],
        [
          "@semantic-release/release-notes-generator",
          {
            "preset": "conventionalcommits"
          }
        ],
        "@semantic-release/gitlab"
      ]
    }
    ```
1.  Ensure your `.gitlab-ci.yml` file includes a `release` stage. This stage should be one of the last in your pipeline, after the build stage. You can provide a different stage name if you don't want to use `release`.
1.  Include `semantic-release.yml` from your `.gitlab-ci.yml` file:

    ```yaml
    include:
      # Analyze commits to determine whether to cut a release
      # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/semantic-release.md
      - project: 'gitlab-com/gl-infra/common-ci-tasks'
        ref: v2.63.0  # renovate:managed
        file: semantic-release.yml
        #inputs:
        #  stage: release
        #  vault: access_tokens/${VAULT_SECRETS_PATH}/semantic-release/token@ci
    ```

## Project Access Token

The `semantic_release` job uses a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) to authenticate.

The following places are checked for a token:

* The `SEMANTIC_RELEASE_GITLAB_TOKEN` environment variable, if set.
* The `inputs.vault` Vault path, if provided.
* The well-known `access_tokens/${VAULT_SECRETS_PATH}/semantic-release/token@ci` Vault path.

### Projects managed by gl-infra/infra-mgmt

Projects managed by [gl-infra/infra-mgmt](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/) can take advantage of integration with the `project/gitlab` Terraform module to create an appropriate *Project Access Token*:

```hcl
module "project_example" {
  source  = "ops.gitlab.net/gitlab-com/project/gitlab"
  version = "3.0.1" # renovate:managed

  common_ci_tasks = {
    enabled = true
  }

  vault = {
    enabled   = true
    # …
  }

  # …
}
```

This will create a *Project Access Token* and store it under a well-known path in Vault.

### Unmanaged projects

**Warning:** unmanaged *Project Access Token* will expire, causing disruption and toil.

Manually create a *Project Access Token* with the `api` and `write_repository` scopes.

There are two options for storing the token: Vault (preferred) and GitLab CI variables:

#### Vault

Store the secret in Vault, typically in the `ci` "engine", and provide a path to the secret using the `vault` input when including `semantic-release.yml`. The input uses the short syntax of [`secrets:vault`](https://docs.gitlab.com/ee/ci/yaml/index.html#secretsvault), see the example below.

#### CI Variable

Create a protected, masked `SEMANTIC_RELEASE_GITLAB_TOKEN` environment variable through the CI/CD Variables settings.

This could be a group token if appropriate.

## Example CI include

```yaml
stages:
  - release

include:
  # Analyze commits to determine whether to cut a release
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/semantic-release.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'semantic-release.yml'
    inputs:
      vault: access_tokens/${VAULT_SECRETS_PATH}/semantic-release/token@ci
```

## Examples of Conventional Commit Messages for Triggering Releases

Here are some examples of [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) messages for triggering a new release:

#### Fix, trigger a [Patch Release](https://semver.org/#spec-item-6)

With a `scope`:

```
fix(widget): correctly handle NPE in subtree explorer
```

Without a `scope`:

```
fix: correctly handle NPE in subtree explorer
```

#### New Feature: Trigger [a Minor Release](https://semver.org/#spec-item-7)

With a `scope`:

```
feat(widget): add the git subtree explorer
```

Without a `scope`:

```
feat: add the git subtree explorer
```

#### Breaking Change, trigger a [Major Release](https://semver.org/#spec-item-8)

```
feat: add the git subtree explorer

BREAKING CHANGES: git submodules explorer API removed.
```

## Delayed Releases

Some minor releases, for example dependency upgrades may need a new version release, but can be delayed to allow multiple upgrades to be included in the same release.

For this purpose, the `semantic_release` job will check for the string `[delay release]` (case-sensitive) in the commit-message. When added to the commit message, the release will be delayed by 1 hour, to allow other changes to be batched together, reducing release spinning.
