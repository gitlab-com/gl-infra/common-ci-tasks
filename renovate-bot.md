# [`renovate-bot`](./renovate-bot.yml)

Runs [`renovatebot`](https://docs.renovatebot.com/) against the project to automatically upgrade dependencies.

## Automated Setup

Follow the instructions in **[the Project Setup documentation for Automated Setup](./docs/project-setup.md)** and
ensure that the follow settings are configured on your Project resource:

```terraform

# Enable common-ci-task definitions
common_ci_tasks = {
    enabled      = true
    renovate_bot = true
    ....
}
```

Note that if the `VAULT_SECRETS_PATH` configuration is present, this task will use Vault for resolution of `RENOVATE_GITLAB_TOKEN`. The token is restricted
to only be accessible on the default protected branch.

You can *optionally* set a schedule using cron format string eg.
```terraform

# Enable common-ci-task definitions
common_ci_tasks = {
    enabled      = true
    renovate_bot = true
    renovate_bot_schedule = "0 0 * * 2" # Every Tuesday at midnight UTC
    ....
}
```

## Manual Setup

For legacy project configurations, the following manual setup is required:

1. Create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with `api` and `write_repository` scope
2. Create a GitLab token to use by Renovate. There are multiple options, see [Access Token](#access-token) for details.
3. Lookup the GitHub Personal Access token from [1Password Production Vault](https://gitlab.1password.com/vaults/7xbs54owvjux3cypztlhyetej4/allitems/53z2zuf7urh7hoy3nqeqsei27e) and save it into the CI environment variable `RENOVATE_GITHUB_TOKEN`. Make sure the variable is **Protected** and **Masked**.
4. Create a CI Pipeline Schedule called `Renovatebot` with a daily schedule, eg `0 1 * * *`. Ensure that the CI Pipeline schedule includes a variable, `RENOVATE_SCHEDULED` with a value of `1`.

   Note: Renovate Bot will only run on `gitlab.com`. For projects that are mirrored to other GitLab instances, the task will not run.

### Access Token

There are four ways to configure the access token used by Renovate. There are in descending order of priority:

1. The `vault` input when including `renovate-bot.yml`.
   This input is used verbatim and needs to include the secret's field as well as the vault mount.

   For example:

   ```yaml
     - project: 'gitlab-com/gl-infra/common-ci-tasks'
       ref: v2.63.0  # renovate:managed
       file: renovate-bot.yml
       inputs:
         vault: "gitlab-com/gitlab-com/gl-infra/renovate/renovate-ci/renovate/gitlab-com/token@ci"
   ```
2. The `VAULT_RENOVATE_GITLAB_TOKEN_PATH` variable.
   This variable holds only the vault path; the secret field is `token` and the Vault mount is `ci`.
   This is the recommended way to override the default behavior if required.

   <details>
   <summary>For example, to use a Group Access Token (the main reason to override the default behavior), you'd add something like the following to the [infra-mgmt repository](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/):</summary>

   ```hcl
   module "group_infra_platform_runway_renovate_bot" {
     source  = "ops.gitlab.net/gitlab-com/group/gitlab//modules/access-token"
     version = "1.2.0"

     group_id = module.group_infra_platform_runway.id
     name     = "Runway Renovate Robot"

     scopes       = ["api", "read_registry", "write_repository"]
     access_level = "maintainer"

     expires_after_days = 90
     rotate_after_days  = 60

     vault_kv_secret = {
       store      = true
       name       = "renovate-bot"
       auth_path  = local.vault_auth_path
       group_path = module.group_infra_platform_runway.full_path
     }
   }

   resource "gitlab_group_variable" "infra-platform-runway-renovate-gitlab-token" {
     group             = module.group_infra_platform_runway.id
     key               = "VAULT_RENOVATE_GITLAB_TOKEN_PATH"
     description       = "Path to the group access token used by Renovate in all Runway projects."
     value             = module.group_infra_platform_runway_renovate_bot.vault_secret_path
     protected         = false # Used in MRs opened by Renovate
     masked            = false
     raw               = true
     environment_scope = "*"
   }
   ```
   </details>
3. The `RENOVATE_GITLAB_TOKEN` variable holds the access token directly.
   This option exists primarily for backwards compatibility and its use is discouraged.
   When using this option, ensure that tokens get automatically rotated at least every 90 days.
   We recommend to use the [project access token](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/gitlab/project/-/tree/main/modules/access-token) or [group access token](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/gitlab/group/-/tree/main/modules/access-token) module to manage access tokens in the [infra-mgmt repository](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/).
   Make sure the variable is **Protected** and **Masked**.
4. **Default:** The token is read from the `access_tokens/${VAULT_SECRETS_PATH}/renovate-bot` Vault path, using the field `token` and the Vault mount `ci`.
   We recommend that you use the [GitLab project Terraform module](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/gitlab/project), which configures the project correctly.

   <details>
   <summary>The following (partial) config will set up everything that you need:</summary>

   ```hcl
   module "project_provisioner" {
     source  = "ops.gitlab.net/gitlab-com/project/gitlab"
     version = "5.11.0"

     # Create access tokens for common-ci-task
     common_ci_tasks = {
       enabled = true
     }

     # Store access tokens in Vault
     vault = {
       enabled   = true
       auth_path = local.vault_auth_path
     }
   }
   ```
   </details>

Note: you can use Group Access Tokens and Group CI/CD variables, instead of Project-level ones if required.

## Configuration

1. Ensure that a `validate` and `renovate_bot` stages exists in your `.gitlab-ci.yml` configuration
2. Create a `renovate.json` file in the root of the project. See below for an example configuration:

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": ["gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common"]
}
```

```yaml
stages:
  - validate
  - renovate_bot

include:
  # Upgrades dependencies on a schedule
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: renovate-bot.yml
```

## Annotating `.tool-versions` and `.gitlab-ci-other-versions.yml` to support Renovate

The preset [`gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common`](./renovate-common.json) includes configuration to upgrade ASDF `.tool-versions` and other version changes in `.gitlab-ci-other-versions.yml`. For `.tool-versions`, Renovate [does natively support this](https://docs.renovatebot.com/modules/manager/asdf/) but with some limitations.

For that reason, add comments into `.tool-versions` so that Renovate understands each dependency.  For example:

```shell
golang 1.17.6 # datasource=github-releases depName=golang/go
goreleaser 1.7.0 # datasource=github-releases depName=goreleaser/goreleaser
golangci-lint 1.45.1 # datasource=github-releases depName=golangci/golangci-lint
shellcheck 0.8.0 # datasource=github-releases depName=koalaman/shellcheck
shfmt 3.4.3 # datasource=github-releases depName=mvdan/sh
1password-cli 1.12.4
```

Similarly, for `.gitlab-ci-other-versions.yml`,

```yaml
variables:
  CONTAINER_IMAGE_VERSION_PREFIXED: "v1.13.1" # datasource=gitlab-releases depName=gitlab-com/container-image
```

Renovate supports multiple datasources: <https://docs.renovatebot.com/modules/datasource/>, giving flexibility in how new versions are looked up.

### Post Upgrade Tasks for `.tool-versions`

If a project has an executable `./scripts/renovate-migrations/tool-version-upgrade.sh`, it will be executed automatically with the following environment variables:

- `RENOVATE_POSTUPG_DEP_NAME`: The name of the dependency being updated
- `RENOVATE_POSTUPG_PACKAGE_FILE`: The filename that the dependency was found in
- `RENOVATE_POSTUPG_DEP_TYPE`: The dependency type (if extracted - manager-dependent)

This is necessary because Renovate will only run a single `postUpgradeTasks` for each dependency, if you override it for a package
that is managed by `.tool-versions` it will break the existing script resulting in a merge requist with a failed pipeline.

### Pinning dependency versions

If you need Renovate to pin to a specific version of a dependency, add a `constraints` object into `renovate.json`. Simply removing the manual annotation may not work as Renovate will [automatically detect and update some `asdf` dependencies](https://docs.renovatebot.com/modules/manager/asdf/).

In the below example, we add the following to `renovate.json` to pin Helm:

```json
  "constraints": {
    "helm": "3.12.3"
  }
```

The [constraints](https://docs.renovatebot.com/configuration-options/#constraints) section of the Renovate documentation has additional detail.

### Using `registryUrl` for Helm Chart Renovate Annotations

When annotating for [`helm`](https://docs.renovatebot.com/modules/datasource/#helm-datasource) dependency updates, the special `registryUrl=https://chart/url` syntax can be added to the annotation to allow to configure the appropriate Helm repository to query.

For example, the following annotation can be used for the `kube-prometheus-stack` Helm chart.

```yaml
# datasource=helm depName=kube-prometheus-stack registryUrl=https://prometheus-community.github.io/helm-charts
```

More details can be found in <https://github.com/renovatebot/renovate/issues/6130>.

### Example Annotations

For convenience, here are some common annotations that can be cut-and-pasted into your projects:

| **Tool**                       | **Annotation**                                                                                                   |
|--------------------------------|------------------------------------------------------------------------------------------------------------------|
| `amtool`                       | `# datasource=github-releases depName=prometheus/alertmanager`                                                   |
| `awscli`                       | `# datasource=github-tags depName=aws/aws-cli`                                                                   |
| `checkov`                      | `# datasource=github-releases depName=bridgecrewio/checkov`                                                      |
| `cmctl`                        | `# datasource=github-releases depName=cert-manager/cert-manager`                                                 |
| `direnv`                       | `# datasource=github-releases depName=direnv/direnv`                                                             |
| `gcloud`                       | `# datasource=github-tags depName=GoogleCloudPlatform/cloud-sdk-docker`                                          |
| `glab`                         | `# datasource=gitlab-releases depName=gitlab-org/cli`                                                            |
| `go-jsonnet`                   | `# datasource=github-releases depName=google/go-jsonnet`                                                         |
| `golang`                       | `# datasource=golang-version depName=golang/go`                                                                  |
| `golangci-lint`                | `# datasource=github-releases depName=golangci/golangci-lint`                                                    |
| `goreleaser`                   | `# datasource=github-releases depName=goreleaser/goreleaser`                                                     |
| `helm`                         | `# datasource=github-releases depName=helm/helm`                                                                 |
| `iam-policy-json-to-terraform` | `# datasource=github-releases depName=flosell/iam-policy-json-to-terraform`                                      |
| `jb`                           | `# datasource=github-releases depName=jsonnet-bundler/jsonnet-bundler`                                           |
| `jq`                           | `# datasource=github-releases depName=jqlang/jq`                                                                 |
| `jsonnet-tool`                 | `# datasource=gitlab-releases depName=gitlab-com/gl-infra/jsonnet-tool`                                          |
| `kubeconform`                  | `# datasource=github-releases depName=yannh/kubeconform`                                                         |
| `kube-prometheus-stack`        | `# datasource=helm depName=kube-prometheus-stack registryUrl=https://prometheus-community.github.io/helm-charts` |
| `kubectl`                      | `# datasource=github-releases depName=kubernetes/kubernetes`                                                     |
| `nodejs`                       | `# datasource=node depName=nodejs/node`                                                                          |
| `pmv`                          | `# datasource=gitlab-releases depName=gitlab-com/gl-infra/pmv`                                                   |
| `poetry`                       | `# datasource=pypi depName=poetry`                                                                               |
| `pre-commit`                   | `# datasource=github-releases depName=pre-commit/pre-commit`                                                     |
| `promtool`                     | `# datasource=github-releases depName=prometheus/prometheus`                                                     |
| `python`                       | `# datasource=github-tags depName=python/cpython`                                                                |
| `ruby`                         | `# datasource=ruby-version depName=ruby/ruby`                                                                    |
| `saml2aws`                     | `# datasource=github-releases depName=Versent/saml2aws`                                                          |
| `shellcheck`                   | `# datasource=github-releases depName=koalaman/shellcheck`                                                       |
| `shfmt`                        | `# datasource=github-releases depName=mvdan/sh`                                                                  |
| `tenctl`                       | `# datasource=gitlab-releases depName=gitlab-com/gl-infra/gitlab-dedicated/tenctl`                               |
| `terra-transformer`            | `# datasource=gitlab-releases depName=gitlab-com/gl-infra/terra-transformer`                                     |
| `terraform`                    | `# datasource=github-releases depName=hashicorp/terraform`                                                       |
| `terraform-docs`               | `# datasource=github-releases depName=terraform-docs/terraform-docs`                                             |
| `tflint`                       | `# datasource=github-releases depName=terraform-linters/tflint`                                                  |
| `thanos`                       | `# datasource=github-releases depName=thanos-io/thanos`                                                          |
| `vault`                        | `# datasource=github-releases depName=hashicorp/vault`                                                           |
| `yarn`                         | `# datasource=github-tags depName=yarnpkg/yarn`                                                                  |
| `yq`                           | `# datasource=github-releases depName=mikefarah/yq`                                                              |

## Forcing Renovate to run off-schedule

By default Renovate will open most MRs (except for vulnerability patches and internal dependency upgrades) over the weekend.
To force Renovate to create MRs immediately, run a pipeline with `RENOVATE_IMMEDIATE=1` set.
This will override the default Renovate schedule. If using vault secrets this will only work on the default protected branch.
## Disabling Renovate schedule

To disable Renovate, run a pipeline with `RENOVATE_DISABLED=1` set.

## Truncated Versions

In an effort to reduce Renovate noise, and toil for Infrastructure teams, for certain low-risk dependencies,
a preset Renovate configuration is available which will configure Renovate to exclude Patch versions
from the specified dependency version.

For example, instead of `3.4.4` as a version, Renovate will suggest `3.4`.

In terms of semver, Renovate will publish `<major>.<minor>` version numbers for these versions,
and the latest `<patch>` version will always be used.

This avoids the need for Renovate MRs for each individual patch upgrade.

In order for this to work effectively and safely, several conditions must be met.

1. Any container/docker images referenced with the truncated version, ie, through the `GL_ASDF_` variables in `.gitlab-ci.yml`,
   should support truncated version tags being published.
   This is not the case for all dependencies, although it is the case for images published in the
   [common-ci-tasks-images project](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images),
   used by several of the tasks in this project.
1. Truncated versions only works with `mise`, since `asdf` does not support truncated versions.
1. Truncated versions should be avoided for runtime dependencies, where having greater control over the providence
   of the produced artifacts is more important.
1. Truncated versions should be avoided for projects that do not strictly follow semver semantics.

This is enabled by default by extending from `gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common`
in a project's `renovate.json` file.

### Disabling Truncated Versions Preset in your Renovate Configuration

Extend your configuration with the `gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-no-truncated-versions` preset.

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common",
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-no-truncated-versions"
  ]
}
```

This will make the listed dependencies use regular semver (`x.y.z`)
version numbers again on renovate runs.

### Publishing Truncated Versions in Upstream Dependencies

#### For Projects that Rely on Git Tags for Versioning

Upstream projects that want to support truncated version publishing can add
the `semantic-release-major-tag` plugin to their Semantic Releaser (`.releaserrc.json`) configuration.
For an example of this in action, check the [`.releaserrc.json`](./.releaserc.json) configuration of this project.

```json
  ["semantic-release-major-tag", {
    "customTags": [
      "v${major}",
      "v${major}.${minor}"
    ]
  }]
```

#### Projects that use Goreleaser to Publish a Container Image

For Goreleaser, configure additional `image_templates` for the `v{{ .Major }}` and `v{{ .Major }}.{{ .Minor }}"` tags,
as shown in this example:

```yaml
dockers:
  - id: something
    image_templates:
      - "{{ .Env.CI_REGISTRY_IMAGE }}:latest"
      - "{{ .Env.CI_REGISTRY_IMAGE }}:{{ .Tag }}"
      # Publish truncated version tags to docker
      # See https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.md#truncated-versions
      # for further information
      - "{{ .Env.CI_REGISTRY_IMAGE }}:v{{ .Major }}"
      - "{{ .Env.CI_REGISTRY_IMAGE }}:v{{ .Major }}.{{ .Minor }}"
```

#### Projects that Publish a Container Image using the [Kaniko Task](./kaniko.md)

Use the `KANIKO_EXTRA_ARGS` variable to pass additional `--destination` arguments to Kaniko:

```yaml
container_image_tagged:
  extends:
      - .kaniko_base
  variables:
    KANIKO_DESTINATION: $CI_REGISTRY_IMAGE/tool:${CONTAINER_TAG}
    KANIKO_EXTRA_ARGS: |
        --destination $CI_REGISTRY_IMAGE/tool:${CONTAINER_MAJOR_MINOR_TAG}
```

See [`common-ci-tasks-images`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/-/blob/main/.gitlab-ci.yml)
for an example of this approach.

## Automerge (Experimental)

In an effort to reduce toil, a [Renovate Automerge configuration](https://docs.renovatebot.com/key-concepts/automerge/) is being experimented with.
To opt-in, add `"gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-automerge"` to the `extends` clause of your `renovate.json`.

> [!NOTE]
> Consider compliance aspects before enabling automerge.
>
> It is typically acceptable to automerge dependency updates if the resulting artifact isn't
> deployed into a production environment automatically and without further review and approval.
>
> See [#3178](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/3178) for further discussion.

This will automatically configure development-time dependency to be auto-merged if the pipeline succeeds.

Automerge is independent of [Truncated Versions](#truncated-versions). They can be used together, or separately.

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common",
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-automerge"
  ]
}
```

Approval rules and CODEOWNER settings need to be set up to allow renovate to automerge changes.

The following settings are suggested:

1. Remove any non-optional approval rules (Settings > Merge Requests > Merge request approvals)
1. Setup [CODEOWNER definition](https://docs.gitlab.com/ee/user/project/codeowners/) to require approvals for regular changes and use zero-approvers for files used for dependency management (see example below)
1. Require CODEOWNER approval on protected branch (Settings > Repository > Protected Branches > Code Owner Approval)
1. Enable "Pipelines must succeed" (Settings > Merge Requests > Merge Checks) to enforce passing CI before automerging
1. Optional: Consider using [custom package rules](https://docs.renovatebot.com/configuration-options/#packagerules) in addition to the default rules in `renovate-automerge.json`

Example `CODEOWNER` definition:

```
# Require approval by certain group
* @gitlab-org/scalability

# Use zero-approvers for files related to dependency management
requirements.txt
.gitlab-ci-*-versions.yml
```

> [!NOTE]
>
> A user or group referenced as a codeowner must be a direct member of the GitLab project,
> [otherwise the rule doesn't apply](https://docs.gitlab.com/ee/user/project/codeowners/#approvals-shown-as-optional).


## Standard Version Constraints

By default, Renovate will attempt to update all versions to the latest. However, there may be external constraints on versions. For example, the version of Kubernetes dependencies should not exceed the version of Kubernetes running in production by more than one minor version.

To DRY-up the control of these version constraints, two presets are available:

1. **For GitLab.com version constraints**: `"gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-versions-gitlab-com"`.
1. **For GitLab Dedicated version constraints**: `"gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-versions-dedicated"`.

Please ensure that these dependencies are kept in sync with infrastructure by updating the dependencies when upgrades to infrastructure is carried out.

## Terraform Module Management

It's considered a security best practice to pin Terraform modules to a specific version to ensure that any changes to the module are reviewed and approved before being applied to production. Additionally, as tags are considered mutable in Git, it's recommended to leverage the digest of a particular version as opposed to the tag. Using the digest will help prevent [supply chain attacks](https://medium.com/boostsecurity/erosion-of-trust-unmasking-supply-chain-vulnerabilities-in-the-terraform-registry-2af48a7eb2) or any unintentional changes by the module author.

To allow Renovate to manage updating the terraform modules pinned to digest, simply annotate the `file.tf` with the following:

```hcl
module "project_services" {
  #renovate: version=v14.2.0
  source = "git::https://github.com/terraform-google-modules/terraform-google-project-factory.git//modules/project_services?ref=84ade5e6a32241128ac39a66dd5bf6184eb84043"
}
```

By default this Renovate config leverages the `github-tags` datasource to look for updates but if your module is hosted elsewhere or requires a different datasource, simply update the annotation like so:

```hcl
module "project_services" {
  #renovate: version=v14.2.0 datasource=gitlab-tags
  source = "git::https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security-public/oidc-modules.git//terraform-modules/gcp-oidc?ref=2cb4111e026b3a18ff1271a4665b3015a278ea22"
}
```

> [!NOTE]
>
> The `regexManager` used to support this is a temporary workaround until Renovate supports this natively.
>
> Currently Renovate partially supports `mise` and `asdf`, although not all plugins are supported.
>
> See [this issue](https://github.com/renovatebot/renovate/issues/23248) for more details and up-to-date status.
