#!/usr/bin/env bash

# This file is referenced from renovate-common.json, in the
# postUpgradeTasks.commands, and is intended to be called within
# the renovate container image, following an upgrade.
# Because postUpgradeTasks can't be merged together, we run all
# common upgrade tasks from a single script.
#
# The script takes a two arguments, `{{{depName}}}` and `{{{packageFile}}`
# which are resolvedby renovate:
# https://docs.renovatebot.com/templates/
#
# ASDF_DIR should be configured, but will default to /asdf

set -euo pipefail
IFS=$'\n\t'

dep_name=$1
package_file=$2
script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

main() {
  echo "post-renovate-upgrade.sh updating repository..."

  if [[ -x ./scripts/update-asdf-version-variables.sh ]]; then
    echo "Running ./scripts/update-asdf-version-variables.sh..."

    ./scripts/update-asdf-version-variables.sh
  fi

  if [[ -x ./scripts/renovate-migrations/tool-version-upgrade.sh ]]; then
    echo "Running ./scripts/renovate-migrations/tool-version-upgrade.sh..."

    env RENOVATE_POSTUPG_DEP_NAME="$dep_name" \
      RENOVATE_POSTUPG_PACKAGE_FILE="$package_file" \
      ./scripts/renovate-migrations/tool-version-upgrade.sh
  fi

  # When shfmt is updated, rerun the command to apply any new rules
  if { [[ $dep_name == 'mvdan/sh' ]] || [[ $dep_name == 'shfmt' ]]; } && [[ $package_file == '.tool-versions' ]]; then
    echo "Running ${script_dir}/renovate-upgrade-scripts/shfmt.sh..."

    "${script_dir}/renovate-upgrade-scripts/shfmt.sh"
  fi

  if [[ $dep_name == "golangci/golangci-lint" ]] && [[ $package_file == '.tool-versions' ]]; then
    echo "Running ${script_dir}/renovate-upgrade-scripts/golangci-lint.sh..."

    "${script_dir}/renovate-upgrade-scripts/golangci-lint.sh"
  fi

  # If Terraform required_provider is upgraded, and the project uses terraform-docs, re-run the docs
  # for updated documentation that remains up-to-date
  if [[ ${RENOVATE_POSTUPG_DEP_TYPE-} == "required_provider" ]] && [[ -x "./scripts/generate-terraform-docs.sh" ]]; then
    mise install terraform-docs
    eval "$(mise activate bash --shims)"

    echo "Reapplying ./scripts/generate-terraform-docs.sh for updated documentation."

    ./scripts/generate-terraform-docs.sh || true # Don't treat a failed exit code as a script failure
  fi
}

main
