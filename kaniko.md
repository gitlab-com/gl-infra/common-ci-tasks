# Kaniko Extensible Tasks

**DEPRECATION NOTICE**: ⚠️⚠️the Kaniko task definition is deprecated in favour of the Docker task.
Please migrate to the Docker build task instead.
<https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docker.md>

## [`.kaniko_base`](./kaniko.yml)

This is not a full template, but allows users to easily define their own [kaniko](https://github.com/GoogleContainerTools/kaniko) container image builds without all the boilerplate.

```yaml
include:
  # Includes a base template for running kaniko easily
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/kaniko.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'kaniko.yml'

.container_builds:
  stage: release
  variables:
    # KANIKO_BUILD_FILE: the Dockerfile to build
    KANIKO_BUILD_FILE: Dockerfile.alt # Defaults to Dockerfile
    # KANIKO_BUILD_CONTEXT: the build context to use: https://github.com/GoogleContainerTools/kaniko/blob/main/README.md#kaniko-build-contexts
    KANIKO_BUILD_CONTEXT: sub/directory/ # Defaults to project root
    KANIKO_EXTRA_ARGS: |
      --build-arg GL_ASDF_TFLINT_VERSION
      --build-arg GL_ASDF_GOLANG_VERSION
      --build-arg GL_DEDICATED_CONTAINER_IMAGE_VERSION_PREFIXED
  extends:
    - .kaniko_base

container_image_build:
  variables:
    KANIKO_DESTINATION: $CI_REGISTRY_IMAGE/onboard:${CI_COMMIT_REF_SLUG}
  extends:
    - .container_builds
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'

# Tags use the git tag, not the slug
container_image_tagged:
  variables:
    KANIKO_DESTINATION: $CI_REGISTRY_IMAGE/onboard:${CI_COMMIT_TAG}
  extends:
    - .container_builds
  rules:
    - if: '$CI_COMMIT_TAG'
```

### Caching

By default, caching is enabled for Kaniko. This can be disabled by setting the `KANIKO_DISABLE_CACHING` to any non-empty value.

```yaml
container_build:
  stage: release
  variables:
    # ...
    KANIKO_DISABLE_CACHING: "true"
  extends:
    - .kaniko_base
```

## [`.kaniko_appsec_scan`](./kaniko.yml)

This task can be used to trigger a pipeline for scanning container images and include the container scanning results in the project that is triggering this scan. The pipeline is triggered in the [`container-scanners`](https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/) project which is used, maintained by the GitLab Appsec team. `CONTAINER_SCAN_PIPELINE_TRIGGER_TOKEN` and `CONTAINER_SCAN_PROJECT_API_TOKEN` CI/CD variables need to be configured on the project that is triggering this scan. See [this section](https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/tree/master/#how-to-trigger-a-new-container-scan-and-include-results-in-the-target-project) for more information. These variables should already be available to projects under `gl-infra` subgroup as they were added as Group CI/CD variables.

```yaml
container_image_scan:
  stage: validate
  variables:
    IMAGES: "Set this to be the names of the images you wish to scan, comma delimited"
  needs:
    - container_image_build
  extends:
    - .kaniko_appsec_scan
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
```
