# [`conftest`](./conftest.yml)

Runs [`conftest`](https://www.conftest.dev/) to test configuration files against OPA policies.

```yaml
stages:
  - validate

include:
  # Test configuration files against OPA policies
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/conftest.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: conftest.yml

variables:
  # Set to the directories or files to test
  CONFTEST_FILES: terraform/
  # Set to the directory containing the OPA policies (default: policy)
  CONFTEST_POLICY: policy
```

> [!tip] Conftest options
> Most Conftest options can be set with `CONFTEST_` environment variables, see [here](https://www.conftest.dev/options/) for more information.

## Unit tests

Rego unit tests named `*_test.rego` will be executed in the `conftest:verify` job.
