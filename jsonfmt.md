# [`jsonfmt`](./jsonfmt.yml)

Validates that all the target JSON files are canonically formatted, with sorted keys.

Setup process:

1. Ensure `PATHS_TO_JSON` is configured to set the path(s) to the JSON files you wish to ensure are canonically formatted. This can be a list of directories or a combination of relative paths to JSON files. All paths to files must include the `.json` extension.
2. Add the `jsonfmt` job via an include.

```yaml
variables:
  PATHS_TO_JSON: "json_dir/filename1.json" "json_dir/filename2.json" "json_dir2"

stages:
  - validate

include:
  # Validates that all target JSON files in the git repository are formatted in a
  # canonical manner with sorted keys
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/jsonfmt.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: jsonfmt.yml
```
