# Pre-Commit Tasks

`common-ci-tasks` provides some `pre-commit` tasks specifically designed for GitLab Infrastructure usage.

## Introduction

### Why run `pre-commit`?

`pre-commit` allows you and other engineers to catch straight-forward problems faster,
allowing the problems to be corrected before being pushed to GitLab CI.
This can significantly improve the productivity of engineers as they don't need to wait for CI jobs to complete
and then parse the logs to uncover the failures.

Usage of `pre-commit` is encouraged but not mandatory.
Developers are welcome to opt-out of this by uninstalling the hooks, or
by using `git commit --no-verify` on a commit-by-commit basis.

Any verification done by `pre-commit` should also always be done using
GitLab CI/CD tasks.

### Why maintain your own set of `pre-commit` tasks?

1. Since we can publish tags for the repository,
   we can provide compatibility with truncated versions `<major>`.`<minor>` version tags.
   This will reduce Renovate MR churn.
1. No need for multiple sources of truth for version numbers in `.tool-versions` and `.pre-commit-hooks.yaml`,
   and no need to keep these versions synchronized,
   which adds complex and can result in more Renovate MRs for each source.
1. No need for multiple installs of tools (one for mise, another for pre-commit).
   The `common-ci-tasks` always rely on the existing `mise` install and will not install another copy of the tool.
1. Since `common-ci-tasks`  hooks rely exclusively on `mise` for tools.
   Thus, fewer failures when installing hooks, etc.
1. Fewer dependencies. No need to rely on external repositories, some of which may be derelict.
1. Easier synchronization between the way tools are used in CI/CD and in pre-commit:
   less chance of these two configurations being different.
1. No need to copy complex hook configurations into many different projects
1. Smaller security surface. The possibility of security breaches through hooks exists,
   so maintaining these internally reduces risk.
1. The hook configurations are generally very straight-forward,
   so not a lot of investment is needed to make them internal.

## Using the `pre-commit` hooks

The hooks are defined in [`.pre-commit-hooks.yaml`](../.pre-commit-hooks.yaml).

The full set of hooks is listed. Individual hooks can be removed depending on the project.
For example, the `go-*` hooks can be removed for non-Golang projects.

```yaml
# .pre-commit-config.yaml
repos:
  # pre-commit tasks from common-ci-tasks
  - repo: https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks
    rev: v2.29.0  # renovate:managed
    hooks:
      - id: go-fmt  # Run gofmt for changed Go files
      - id: go-imports  # Run go-imports for changed Go files
      - id: go-mod-tidy  # Run go mod tidy for changed Go files
      - id: go-test  # Run go test for the project if any go files have changed
      - id: golangci-lint  # Run golangci-lint for changed Go files
      - id: shellcheck  # Run shellcheck for changed Shell files
      - id: shfmt  # Run shellcheck for changed Shell files
      - id: update-asdf-version-variables  # Update .gitlab-ci-asdf-versions.yml if .tool-versions has changed
      - id: gitlint  # Checks your git commit messages for style
      - id: gitleaks  #  Detect hardcoded secrets using Gitleaks
      - id: editorconfig  # Validate text files against .editorconfig
      - id: yamllint  # Validate YAML files using yamllint
      - id: terraform-fmt  # Apply terraform fmt across all changed terraform files
      - id: terraform-tflint  # Run tflint on changed modules
      - id: terra-transformer-validate-roots  # Run terra-transformer validate on root modules
      - id: jsonnet-fmt  # Apply jsonnetfmt across all changed jsonnet files
```
