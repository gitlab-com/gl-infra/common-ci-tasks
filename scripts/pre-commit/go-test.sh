#!/usr/bin/env bash

#
# Run go test
#
# This runs on all_files, since Go is better at figuring out dependencies than pre-commit
#

set -euo pipefail
IFS=$'\n\t'

eval "$(mise activate bash --shims)"

go test -short ./...
