# Environment Setup

This page describes the common process for setting up the common local development and workstation setup used across many Infrastructure projects.

## Setup

### Step 0: Install Homebrew \[Mac OS X only\]

Install the *Homebrew* package manager for Mac OS X.

*   Head to https://github.com/Homebrew/brew/releases and download a recent release as a `.pkg` (installer package).
*   Execute the installer to install Homebrew.
*   Add the Homebrew `bin/` directory to your path, e.g. by adding the following to `~/.zshenv`:

    ```shell
    typeset -U path
    path=($path /opt/homebrew/bin)
    ```

### Step 1: Install 1password-cli v2

In order to protect sensitive values, many GitLab projects rely on 1password for storing secrets. This may be either directly, or through internal tooling such as [`pmv`](https://gitlab.com/gitlab-com/gl-infra/pmv).

Install 1password CLI using the instructions at: <https://developer.1password.com/docs/cli/get-started#install>.

For MacOS, you can use:

```shell
brew install 1password-cli
```

Once 1password-cli is installed you can verify the installation using:

```shell
# Check that version >= 2
op --version

# Ensure that you are signed into GitLab
eval $(op signin --account gitlab.1password.com)

# Check that the connection is working
op vault list
```

**Note: v2 of the 1password-cli is required.**

#### Optional but recommended: Enable Biometrics

Enabling Biometrics in the 1password-cli reduces the need to enter your password over and over again.

1. Open and unlock the 1Password app.
1. Navigate to **Settings > [Security](onepassword://settings/security)**.
1. Turn on **Touch ID**.
1. Navigate to **Settings > [Developer](onepassword://settings/developers)**.
1. Turn on **Integrate with 1Password CLI**.

### Step 2: Setup Development Libraries

The Infrastructure group uses some Python-based tools, including the [`gcloud` CLI](https://cloud.google.com/sdk/gcloud) and the [AWS CLI](https://aws.amazon.com/cli/), [`pre-commit`](https://pre-commit.com/) and [Ansible](https://www.ansible.com/).

We've found that in some cases, Python will silently drop required modules (eg `sqlite3`) when the library dependencies of those modules are not installed on your system. This can lead to systems failing in unusual ways at runtime.

It is recommended that you follow the instructions to install the suggested build environment for Python, here: <https://github.com/pyenv/pyenv/wiki#suggested-build-environment>.

Linux users can follow the instructions for their distro on that site. For Macos users, the following set of commands should ensure the prerequisites.

```shell
# Ensure that xcode build tools are installed
xcode-select --install
# Ensure required libraries for python
brew install openssl readline sqlite3 xz zlib bash
```

### Step 2.1: Macos Only: you're gonna need a better shell

In the previous command we also installed a modern version of `bash`.
This is because macOS ships with an very outdated version (circa 2007) of `bash`, due to licensing issues.
Homebrew should install a modern version (at least v5).

Alongside _installing_ a modern `bash`,
you'll also need to make sure that you're _using_ a modern `bash` as your login shell.
Configure your terminal to use the homebrew install (likely `/opt/homebrew/bin/bash`) for new sessions,
instead of the default, `/bin/bash`. If you run bash, you can check the version with `echo $BASH_VERSION`,
and making sure it's at least v5.

You can also configure your default macos login shell to use the newer version of `bash`, by changing your shell, as follows:

1. `sudo vi /etc/shells` and add a line for `/opt/homebrew/bin/bash` (or wherever your `bash` resides) to the end of the file.
2. `chsh -s /opt/homebrew/bin/bash` to configure your login to use the new bash.

Alternatively, use a different shell, eg `chsh -s /bin/zsh`.

### Step 2.5: Apple Silicon users only: install Rosetta

**Note**: this step is under review: please confirm whether this is still required in <https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/5272> before proceeding.

> To determine if you are using an Apple with Silicon or an Intel-based processor, select the **Apple** menu icon in the top-left of the desktop and click **About This Mac**. On Mac computers with Apple silicon, About This Mac shows an item labeled **Chip**, followed by the name of the chip (ex. **Chip** Apple M1 Pro). On Mac computers with an Intel processor, About This Mac shows an item labeled **Processor**, followed by the name of an Intel processor.

If you are running Apple Silicon it's highly recommended that you install Rosetta. Many downstream tools still generate `amd64` binaries only, and even if the tools support `arm64`, `asdf` plugins also need to support it, which they may not yet.

**Note**: You will likely hit many problems unless Rosetta is installed.

Check if Rosetta is installed by checking if it is running with the following one-liner:

```shell
pgrep -q oahd && echo "Rosetta is present on the system and is running." || echo "Rosetta is not running."
```

If it is not running, it is likely that it is not installed, so continue with the installation steps:

```shell
softwareupdate --install-rosetta --agree-to-license
```

### Step 3.1: Uninstall `asdf` before installing `mise`

The Infrastructure department is currently migrating from `asdf` to `mise` (formerly known as `rtx`). See [gitlab-com/runbooks#134](https://gitlab.com/gitlab-com/runbooks/-/issues/134) for details of the migration. Many projects now support both `mise` and `asdf`, but if you're experiencing errors running `scripts/install-asdf-plugins.sh` on a project, please leave a comment on [gitlab-com/runbooks#134](https://gitlab.com/gitlab-com/runbooks/-/issues/134).

**It is not recommended that you run `asdf` alongside `mise`.**

Please ensure that you uninstall `asdf` before installing `mise`.

To uninstall `asdf`, full instructions are available at <https://asdf-vm.com/manage/core.html#uninstall>.

For MacOS users who installed `asdf` with Homebrew, remove any includes of `asdf` from your Shell initialization, then use Homebrew to remove `asdf`:

```console
$ # find asdf references in your shell rc files
$ grep -Er 'asdf.(ba)?sh' ~/.bash_profile ~/.bashrc ~/.zshrc ~/.oh-my-zsh/custom/*
$ # edit the files to delete any references
$ vi ...
$ # now, uninstall asdf
$ brew uninstall asdf --force
$ # open a new terminal before continuing...
```

In addition to removing references to loading asdf in your rc files, you may need to remove compile time environment variables such as LDFLAGS, RUBY_CONFIGURE_OPTS, CPPFLAGS, and PKG_CONFIG_PATH.

### Step 3.2: Install `mise`

[`mise`](https://github.com/jdx/mise) is a polyglot runtime manager. It is compatible with [`asdf`](https://asdf-vm.com/) and relies on the `asdf` plugin ecosystem, but has some advantages over `asdf` in that it generally requires fewer shims and is faster.


If you're running on MacOS, the recommended approach is to use Homebrew:

```shell
brew install mise
```

Linux users should follow the instructions for their package manager in [the mise documentation](https://mise.jdx.dev/getting-started.html#apt).

#### Step 3.3: Hook `mise` into your shell

Once you've installed `mise`, add the following line to your shell. Remember to restart your terminal for the change to take affect.

```shell
# Disable the `legacy_version_file` feature for mise
mise settings set legacy_version_file false

# bash
echo 'eval "$('$(which mise)' activate bash)"' >> ~/.bashrc
# zsh
echo 'eval "$('$(whence -cp mise)' activate zsh)"' >> ~/.zshrc
# fish
echo 'mise activate fish | source' >> ~/.config/fish/config.fish
```

Did you remember to restart your terminal? Good.

#### Step 3.4: Setup `direnv`

>>>
`direnv` is an extension for your shell. It augments existing shells with a new feature that can load and unload environment variables depending on the current directory.
>>>

Follow the `direnv` [installation documentation](https://github.com/direnv/direnv/blob/master/docs/installation.md). For macOS, you can use:

```shell
brew install direnv
```

Next, you'll need to hook `direnv` into your shell.

```shell
# bash
echo 'eval "$(direnv hook bash)"' >> ~/.bashrc
# zsh
echo 'eval "$(direnv hook zsh)"' >> ~/.zshrc
```

For other shells, follow the instructions at <https://github.com/direnv/direnv/blob/master/docs/hook.md>.

#### Step 3.5: Setup `cosign`

#### cosign

Cosign a tool used for signature verification for blobs and docker images.
We use it to verify the authenticity and provenance of the binaries we produce.

```shell
# Install the latest cosign v2.*.* and make it the global default
mise install --force cosign 2; mise global cosign 2
```

### Step 4: Install development dependencies

Install all the plugins by running:

```shell
./scripts/prepare-dev-env.sh
```

This will install required `asdf`/`mise` plugins, and install the correct versions of the development tools.

Note that after pulling changes to the repository, you may sometimes need to re-run `./scripts/prepare-dev-env.sh` to reconfigure your locally development environment.

### Step 5: Macos Only: give your shell full disk access

Since macOS 14.10 the security posture of Apple increased release by release. Certain paths of your disk require explicit permissions in order for applications to access them.

It is possible to grant full-disk access to your terminal, to avoid being asked each time, [this guide](https://gitlab.com/gnachman/iterm2/-/wikis/fulldiskaccess) explains how to enable it for iTerm2 but can easily be adapted for each terminal application.

## Updating Tool Versions

We use CI checks to ensure that tool versions used in GitLab-CI and on developer instances don't go out of sync.

### Keeping Versions in Sync between GitLab-CI and `.tool-versions`.

The `.tool-versions` file is the SSOT for tool versions used in this repository.
To keep `.tool-versions` in sync with `.gitlab-ci.yml`, there is a helper script,
`./scripts/update-asdf-version-variables.sh`.

#### Process for updating a Tool Version

```shell
./scripts/update-asdf-version-variables.sh
```

1. Update the version in `.tool-versions`
1. Run `mise install` to install latest version
1. Run `./scripts/update-asdf-version-variables.sh` to update a refresh of the `.gitlab-ci-asdf-versions.yml` file
1. Commit the changes
1. A CI job (see [`asdf-tool-versions.md`](../asdf-tool-versions.md)) will validate the changes.

# Diagnosing `mise` setup issues

## `asdf`/`mise` plugins don't return versions as expected

To avoid installing dependencies, `asdf`/`mise` plugins often rely on basic Unix text processing utilities like `grep`, `sed` and `awk` to parse JSON. Many rely on the fact that responses from the GitHub API are pretty-printed JSON, not minimised (or machine parsable) JSON. However, the GitHub API will only pretty-print JSON when it detects the User-Agent request header as being `curl`. For other user agents, the response will be minimised for efficiency.

Ensure that you haven't overridden your `curl` user-agent on your workstation.

Check your `.curlrc` for the `user-agent` setting. Additionally, running `curl https://api.github.com/orgs/gitlabhq` should return pretty-printed JSON. If the response contains minimised JSON, many `asdf`/`mise` plugins may not work as expected.

## `asdf`/`mise` plugins require other `asdf`/`mise` plugins

If you find that a plugin is failing to install, it sometimes helps to setup a global default. Care has been taken to avoid this situation, but if you're stuck, give it a try:

```shell
# If a plugin is complaining that it cannot compile because golang hasn't been configured...
# set the global version of golang the same as the current version
mise global golang $(mise current golang)
mise install
```

If this doesn't work, you can try uninstalling then reinstalling the plugin producing the error, for example:

```shell
$ git commit -m "commit message"
Checkov AWS..............................................................Failed
- hook id: checkov
- exit code: 1

Traceback (most recent call last):
  File "installs/checkov/2.5.8/bin/checkov", line 2, in <module>
    from checkov.main import Checkov
  File "installs/checkov/2.5.8/checkov/main.py", line 23, in <module>
    from checkov.ansible.runner import Runner as ansible_runner
  File "installs/checkov/2.5.8/checkov/ansible/runner.py", line 7, in <module>
    from checkov.ansible.graph_builder.local_graph import AnsibleLocalGraph
  File "installs/checkov/2.5.8/checkov/ansible/graph_builder/local_graph.py", line 14, in <module>
    from checkov.ansible.utils import get_scannable_file_paths, TASK_RESERVED_KEYWORDS, parse_file
  File "installs/checkov/2.5.8/checkov/ansible/utils.py", line 9, in <module>
    from checkov.common.parsers.yaml.parser import parse
  File "installs/checkov/2.5.8/checkov/common/parsers/yaml/parser.py", line 8, in <module>
    import checkov.common.parsers.yaml.loader as loader
  File "installs/checkov/2.5.8/checkov/common/parsers/yaml/loader.py", line 10, in <module>
    from checkov.common.util.file_utils import read_file_with_any_encoding
  File "installs/checkov/2.5.8/checkov/common/util/file_utils.py", line 12, in <module>
    from charset_normalizer import from_path
ModuleNotFoundError: No module named 'charset_normalizer'

$ mise uninstall checkov --all
mise checkov@2.5.10                ✓ uninstalled
mise checkov@2.5.8                 ✓ uninstalled

$ mise install
mise checkov@2.5.8                 ✓ installed
```

**Note**: `mise` (formerly known as `rtx`). If you haven't [migrated yet](https://gitlab.com/gitlab-com/runbooks/-/issues/134), replace `mise` with `rtx` in the above commands.

Additionally, please consider opening an issue in the appropriate tracker, so that a better long-term solution can be applied.

## Macos running in Rosetta Emulation Mode

If your shell or terminal application is running in Rosetta Emulation Mode, you're going to have a bad time.
Make sure that you're using a Universal or Apple Silicon terminal program.

You can check if you're running Intel emulation on your shell and terminal using the Activity Monitor,
by reviewing the `Kind` column.
