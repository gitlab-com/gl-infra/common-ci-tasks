local MAJOR_MINOR = "MAJOR_MINOR";
local MAJOR = "MAJOR";

// This is a special case for gitlab-ci and pre-commit which use tags
// instead of semver and REQUIRE the `v` prefix on the version
local MAJOR_WITH_TAGS = "MAJOR_WITH_TAGS";

// Add packages in here if you want to use truncated versions with them..
local config = {
  "gitlab-com/gl-infra/common-ci-tasks": MAJOR_WITH_TAGS,
  "gitlab-com/gl-infra/jsonnet-tool": MAJOR_MINOR,
  "gitlab-com/gl-infra/pmv": MAJOR_MINOR,
  "golangci/golangci-lint": MAJOR_MINOR,
  "goreleaser/goreleaser": MAJOR_MINOR,
  "koalaman/shellcheck": MAJOR_MINOR,
  "mvdan/sh": MAJOR_MINOR,
};

local listPackages(type) =
  std.sort(
    std.flatMap(
      function(p)
        if p.value == type then
          [p.key]
        else
          [],
      std.objectKeysValues(config)
    )
  );

local listAllPackages() =
  std.sort(std.objectFields(config));

{
  MAJOR_MINOR:: MAJOR_MINOR,
  MAJOR:: MAJOR,
  MAJOR_WITH_TAGS:: MAJOR_WITH_TAGS,

  listPackages:: listPackages,
  listAllPackages:: listAllPackages,
}
