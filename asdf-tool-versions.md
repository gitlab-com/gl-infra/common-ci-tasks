
# [`asdf-tool-versions`](./asdf-tool-versions.yml)

Checks that the `.tool-versions` file is synced with .gitlab-ci-asdf-versions.yml, that all the plugins are declared in `./scripts/install-asdf-plugins.sh` and that Mise are generally working.

The name of this template is for backwards compatibility purposes only. The template not longer supports `asdf`, only `mise`.

The [Infrastructure group migration from asdf to mise](https://gitlab.com/gitlab-com/runbooks/-/issues/134) effort changed this template
to only run a job for `mise`: `validate_mise_tool_versions`.

Documentation on `mise` can be found at <https://mise.jdx.dev/>.

* Default Stages: `validate`

Setup process:

1. Ensure that the file [`scripts/install-asdf-plugins.sh`](https://gitlab.com/gitlab-com/runbooks/-/blob/master/scripts/install-asdf-plugins.sh) exists in the repository. Tailor for the `mise` configuration of the project.
1. Ensure that the files [`scripts/update-asdf-version-variables.sh`](https://gitlab.com/gitlab-com/runbooks/-/blob/master/scripts/update-asdf-version-variables.sh) exists in the repository.
1. If the job will need SSH access to GitLab using a [deploy key](https://docs.gitlab.com/ee/user/project/deploy_keys/), create the deploy key for the project to which access will be required. Add the private key to the `VALIDATE_ASDF_TOOL_VERSIONS_DEPLOY_KEY_FILE` CI/CD variable.
    1. Note that the SSH Agent may fail to parse the private key if a final newline is omitted.
    1. To generate the key, follow the instructions at <https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair>.
    1. TL;DR is `ssh-keygen -t rsa -b 2048 -C "asdf-validation deploy key for <project>"`
1. Add the include to your `.gitlab-ci.yml` file:

```yaml
include:
  - local: .gitlab-ci-asdf-versions.yml

  # Checks that the `.tool-versions` file is synced with .gitlab-ci-asdf-versions.yml,
  # that all the plugins are declared in `./scripts/install-asdf-plugins.sh`
  # and that asdf and mise are generally working
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/asdf-tool-versions.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: asdf-tool-versions.yml
```
