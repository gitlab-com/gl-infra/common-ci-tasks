# [`terraform-format`](./terraform-format.yml)

Runs [`terraform fmt`](https://www.terraform.io/cli/commands/fmt) to ensure that all Terraform files are correctly formatted.

1. Ensure that the `GL_ASDF_TERRAFORM_VERSION` version for terraform is configured.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_TERRAFORM_VERSION: ...

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Ensures that all terraform files are correctly formatted
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/terraform-format.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: terraform-format.yml
```
