# yamlfmt

Runs **[yamlfmt](https://github.com/google/yamlfmt)** to format YAML files uniformly.

## Setup

Simply include `yamlfmt.yml` in your `.gitlab-ci.yml` file:

```yaml
stages:
  - validate

include:
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: yamlfmt.yml
```

## Configuration

The *yamlfmt* job provides very limited configuration options. This is by design.

### Including and excluding files

The `.yamlfmt.patterns` file allows to specify additional files and/or exclude files from the format check.
It is sometimes desireable to check the format of files that do not have the usual `.yaml` or `.yml` suffix.
At other times it is necessary to exclude files from the format check, for example intentionally malformed test data.

To include or exclude files, create a file named `.yamlfmt.patterns` at the root of your repository.

Files to **include** are added as normal (positive) entries, for example:

```gitignore
/.some-tool # dotfile using YAML syntax but without the suffix
```

Files to **exclude** are added a negated entries, for example:

```gitignore
!testdata/
```

The `.yamlfmt.patterns` file uses [gitignore](https://git-scm.com/docs/gitignore) syntax.
Despite the name, only negated entries (beginning with an exclamation point `!`) are *excluded*, all other entries are *included*.

The `.yamlfmt.patterns` file is combined with [the default `yamlfmt.patterns` file](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/-/blob/main/yamlfmt/yamlfmt.patterns?ref_type=heads) with the default file coming first and the `.yamlfmt.patterns` file second/below.
