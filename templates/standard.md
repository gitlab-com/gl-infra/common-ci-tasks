# [`templates/standard.yml`](./templates/standard.yml)

This template should be included in all Infrastructure projects. It includes standard checks, gitlab-scanners, validations and release processes common to all projects using this template library.

Includes the following tasks:

1. [`asdf-tool-versions.yml`](../asdf-tool-versions.md): ensures that `.tool-versions` file is synced with .gitlab-ci-asdf-versions.yml, that all the plugins are declared in `./scripts/install-asdf-plugins.sh` and that ASDF is generally working.
1. [`autolabels.yml`](../autolabels.md): adds [Work Type Classification](https://handbook.gitlab.com/handbook/product/groups/product-analysis/engineering/metrics/#work-type-classification) labels to merge requests.
1. [`editorconfig-check.yml`](../editorconfig-check.md): ensures that the repository adheres to the [EditorConfig](https://editorconfig.org/) rules for the project.
1. [`gitlab-scanners.yml`](../gitlab-scanners.md): adds various GitLab SAST, Dependency Scanner, Secret Detection, Licence Scanning, Container Scanning and IAC Scanner tools.
1. [`gitlint.yml`](../gitlint.md): ensures that commit messages adhere to the standards for the project.
1. [`mirroring.yml`](../mirroring.md): assists in setting up the [Woodhouse `gitlab notify-mirrored-mr`](https://gitlab.com/gitlab-com/gl-infra/woodhouse#gitlab-notify-mirrored-mr-subcommand) notifications and optionally (if a token is supplied) blocks on [Woodhouse gitlab `follow-remote-pipeline`](https://gitlab.com/gitlab-com/gl-infra/woodhouse#gitlab-follow-remote-pipeline-subcommand) remote pipelines.
1. [`semantic-release.yml`](../semantic-release.md): runs [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/) on your repository.
1. [`shellcheck.yml`](../shellcheck.md): performs linting for shell scripts using [shellcheck](https://www.shellcheck.net/).
1. [`shfmt.yml`](../shfmt.md): validates that shell-scripts use a canonical formatting.
1. [`yamllint.yml`](../yamllint.md): validates that YAML matches the YAML formatting rules for the project.

```yaml
# Requires stages validate, release and renovate_bot
stages:
  - validate
  - release
  - renovate_bot

include:
  - local: .gitlab-ci-asdf-versions.yml
  # This template should be included in all Infrastructure projects.
  # It includes standard checks, gitlab-scanners, validations and release processes
  # common to all projects using this template library.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/templates/standard.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: templates/standard.yml
```

## Overriding Stages

You can override the stages that a CI/CD job runs in [using inputs](https://docs.gitlab.com/ee/ci/yaml/includes.html#define-input-parameters-with-specinputs).

```yaml
include:
  - local: .gitlab-ci-asdf-versions.yml
  # This template should be included in all Infrastructure projects.
  # It includes standard checks, gitlab-scanners, validations and release processes
  # common to all projects using this template library.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/templates/standard.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.63.0  # renovate:managed
    file: templates/standard.yml
    inputs:
      validate_stage: test  # stage that validation jobs should run in
      release_stage: deploy  # stage that release jobs should run in
      renovate_bot_stage: test  # stage that renovate should run in
```
