local packages = import 'packages.libsonnet';

local base = {
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "description": "DO NOT CHANGE THIS FILE DIRECTLY: edit the source in src/truncated-versions/truncated-versions.jsonnet !!!",

};

local manifest(v) =
  std.manifestJsonEx(v, "  ", "\n") + "\n";

{
  "renovate-no-truncated-versions.json": manifest(base + {
    "packageRules": [{
      "description": "Reverse renovate-truncated-versions.json rules",
      "matchPackageNames": packages.listAllPackages(),
      "extractVersion": "^v?(?<version>\\d+\\.\\d+\\.d+)",
      "versioning": "semver"
    }]
  }),
  "renovate-truncated-versions.json": manifest(base + {
  "packageRules": [{
      "description": "MAJOR.MINOR Truncated Versions",
      "matchPackageNames": packages.listPackages(packages.MAJOR_MINOR),
      "extractVersion": "^v?(?<version>\\d+\\.\\d+)",
      "versioning": "semver-coerced"
    }, {
      "description": "MAJOR Truncated Versions",
      "matchPackageNames": packages.listPackages(packages.MAJOR) + packages.listPackages(packages.MAJOR_WITH_TAGS),
      "extractVersion": "^v?(?<version>\\d+)",
      "versioning": "semver-coerced"
    }, {
      "description": "MAJOR Truncated Versions which rely on Git Tags",
      "matchPackageNames": packages.listPackages(packages.MAJOR_WITH_TAGS),
      "matchManagers": [
        "gitlabci",
        "gitlabci-include",
        "pre-commit"
      ],
      // NOTE: for *_WITH_TAGS, the v is captured in the `version` capture group!
      "extractVersion": "^(?<version>v?\\d+)",
      "versioning": "semver-coerced"
    }]
  })
}
