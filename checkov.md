
# [`checkov`](./checkov.yml)

Runs [`checkov`](https://www.checkov.io/) across the project.

Setup process:

1. Ensure that a [`.checkov.yaml`](https://bridgecrew.io/blog/checkov-config-file-repeatably-support-multiple-environments/) file exists in the root of the project.
1. Ensure that the `GL_ASDF_CHECKOV_VERSION` version for checkov is configured.
1. Checkov reports will be saved as artifacts if the checkov job fails.
1. Checkov reports will also be processed as JUnit XML Reports by GitLab.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_CHECKOV_VERSION: ...

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Runs checkov on all terraform module directories
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/checkov.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: checkov.yml
```
