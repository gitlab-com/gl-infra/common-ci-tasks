
# [`tflint`](./tflint.yml)

Runs [`tflint`](https://github.com/terraform-linters/tflint) across all directories that contain `*.tf` files.

Setup process:

1. Ensure that a [`.tflint.hcl`](https://github.com/terraform-linters/tflint/blob/master/docs/user-guide/config.md) file exists in the root of the project.
1. Ensure that the `GL_ASDF_TERRAFORM_VERSION` version for terraform is configured.
1. Ensure that the `GL_ASDF_TFLINT_VERSION` version for tflint is configured.
1. Directories can be excluded from tflint using the `TFLINT_EXCLUDE_REGEX` variable. See the example below.
1. The task will generate a [junit test output file](https://docs.gitlab.com/ee/ci/unit_test_reports.html) for any failed linter checks.
1. Supports the `COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP` variable, for excluding paths from validation.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_TFLINT_VERSION: ...
  GL_ASDF_TERRAFORM_VERSION: ...
  # Exclude vendor and files directories from validation
  COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP: '^\.(/vendor/|/files/)'

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Runs tflint on all terraform module directories
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/tflint.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: tflint.yml
```

## Determining which Terraform Modules to Run `tflint` on...

This task will run `tflint` with the `--call-module-type=all` option.
This will recurse through all child modules to ensure that the `tflint` rules also apply to them.

Unfortunately there is no canonical way to determine if a module is a root module in Terraform or not,
so that the task can selectively run on root modules only.

As a proxy, this task uses the presence of `.terraform.lock.hcl` to signal that a module is a root module.

This has some implications for users of this task:

1. Be sure to check the `.terraform.lock.hcl` files in for your root modules.
1. Only add `.terraform.lock.hcl` for your root modules.
   If a module is not a root module,
   but has a checked in `.terraform.lock.hcl` file,
   it's recommended that you remove it from git.
2. Any module that exists in the repo,
   but does not have a `.terraform.lock.hcl` file
   _and_ is not connected to the root module,
   either directly or indirectly,
   will not be linted.

Running `tflint` on root modules only is much more efficient and results in a faster builder,
for relatively little cost.
