# [`danger`](./danger.yml)

[Danger](https://github.com/danger/danger) is a tool used for automated merge request review tasks.

It can be useful for reminding merge request contributors and reviewers of tasks which may need to be carried out.

Danger is widely used across GitLab.

```yaml
variables:
  GL_DEDICATED_GITLAB_DANGERFILES_VERSION: "4.7.0" # datasource=gitlab-releases depName=gitlab-org/ruby/gems/gitlab-dangerfiles

include:
  # Run Danger during merge requests to alert on messages, warnings and errors.
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'danger.yml'
    # inputs:
    #   stage: defaults to `validate`
    #   gitlab_dangerfiles_version: "${GL_DEDICATED_GITLAB_DANGERFILES_VERSION}" # Defaults to latest
    #   bundler_group: ci # Install an alternative group with bundler
```

Next, commit a `Dangerfile` in the root of the project.

```ruby
# frozen_string_literal: true

require 'gitlab-dangerfiles'

Gitlab::Dangerfiles.for_project(self) do |dangerfiles|
  # Import all plugins from the gem
  dangerfiles.import_plugins
  dangerfiles.import_dangerfiles(only: %w[simple_roulette])
end
```

This `Dangerfile` will be run on each pipeline in merge requests.

# Token configuration

The `danger` task relies on Vault tokens to be configured using the GitLab Project Module.

The `common_ci_tasks.danger` configuration must be set to `true`.

Review [the Project Setup documentation](./docs/project-setup.md) for further details.
