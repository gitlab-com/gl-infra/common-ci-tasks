# Docker Extensible Tasks

## [`.docker_buildx_base`](./docker.yml)

This is not a full template, but allows users to easily define their own [docker buildx](https://docs.docker.com/reference/cli/docker/buildx/build/) container image builds without all the boilerplate.

The following variables can be configured:

* `DOCKER_DESTINATION`: **Required**. The destination to push the tag to.
* `DOCKER_BUILD_FILE`: The Dockerfile to build. Defaults to `Dockerfile`.
* `DOCKER_BUILD_CONTEXT`: The context to use. Defaults to `.`.
* `DOCKER_BUILDX_EXTRA_ARGS`: Additional arguments to add to the `docker buildx build` command.
* `GL_VERSION_YML_FILES`: A set of `.gitlab-ci.yml` include files containing `variables`.
   These variables will be passed through as `--build-args`.
   Defaults to `.gitlab-ci-asdf-versions.yml .gitlab-ci-other-versions.yml`.

### Usage Example

```yaml
include:
  # Includes a base template for running an opinionated docker buildx build
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docker.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'docker.yml'

.container_builds:
  stage: release
  variables:
    # DOCKER_BUILD_FILE: the Dockerfile to build
    DOCKER_BUILD_FILE: Dockerfile.alt # Defaults to Dockerfile
    # DOCKER_BUILD_CONTEXT: the build context to use
    DOCKER_BUILD_CONTEXT: sub/directory/ # Defaults to project root
    # DOCKER_BUILDX_EXTRA_ARGS: additional arguments for docker buildx build
    DOCKER_BUILDX_EXTRA_ARGS: |
      --build-arg GL_DEDICATED_CONTAINER_IMAGE_VERSION_PREFIXED
  extends:
    - .docker_buildx_base

container_image_build:
  variables:
    DOCKER_DESTINATION: $CI_REGISTRY_IMAGE/onboard:${CI_COMMIT_REF_SLUG}
  extends:
    - .container_builds
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'

# Tags use the git tag, not the slug
container_image_tagged:
  variables:
    DOCKER_DESTINATION: $CI_REGISTRY_IMAGE/onboard:${CI_COMMIT_TAG}
  extends:
    - .container_builds
  rules:
    - if: '$CI_COMMIT_TAG'
```

### Signing and Verification

The `.docker_buildx_base` task signs container images using keyless signatures based on OIDC tokens.

The [Sigstore project](https://www.sigstore.dev/)  provides a CLI called
[Cosign](https://docs.sigstore.dev/signing/quickstart/) which is used for keyless
signing of container images built with GitLab CI/CD.

This allows certificates to be verified without the need for managing secret keys.

Following the signing of the OIDC-based certificate identity that was used to sign the container is displayed in
logs:

```
------------------------------------------------------------
Verify this container image using:
cosign verify registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks/asdf:v2.63.0 \
  --certificate-identity https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks//.gitlab-ci.yml@refs/tags/v1.2.3 \
  --certificate-oidc-issuer https://gitlab.com
------------------------------------------------------------
```

This can be used to verify the container, without the need for keys.

To read more about keyless verification, using OpenID Connect (OIDC),
please review [the Sigstore docs](https://docs.sigstore.dev/verifying/verify/#keyless-verification-using-openid-connect).

### Caching

By default, caching is enabled for Docker tasks.

Caching will store cache images in the `$CI_REGISTRY_IMAGE/cache` registry.
All cache tags start with the prefix of `cache-` to allow for easy cleanup.

Two cache key schemes are used:

1. `BRANCH_REGISTRY_CACHE_KEY` for branches
2. `DEFAULT_BRANCH_REGISTRY_CACHE_KEY` - for default branch.

Please review the [template definition](./docker.yml) for details,
but these can be overridden depending on project requirements.

There are three caching modes:

1. **For branches**:  `docker buildx` will read from the branch cache and the default branch cache.
   For performance reasons, branches don't write to the cache by default,
   but this can optionally be turned on.
   See [Branch Write Caching](#branch-write-caching) for more information.
1. **For main branch**:  `docker buildx` will read from and write to the default branch cache.
1. **For tags**: `docker buildx` will read from the default branch cache and not write to the cache.

#### Branch Write Caching

Writing cached images to the registry can be very slow:
often slower than a rebuild if the main cache  is relatively warm.

For that reason, writing docker builds to the branch cache is disabled by default.

It can be enabled using the ~docker-write-branch-cache label on your MR,
or adding by the `DOCKER_WRITE_BRANCH_CACHE="1"` variable.

If you're finding that Docker builds on a branch are slow and could benefit from writing the cache,
for maximum efficiency, turn branch write caching on temporarily,
run a successful pipeline,
and then turn branch write caching off again.
This will allow the branch cache to be populated,
while avoiding the costly cache write at the end of each docker build job.

Note that Docker builds will attempt to read the branch cache even when branch cache writing is disabled.

### SBOM Attestation

By default, tag and default branch images will have SBOM attestations attached using the Docker SBOM
attestation feature: <https://docs.docker.com/build/metadata/attestations/sbom/>.

This behaviour can be modified:

1. Add the ~docker-attest-sbom label to an MR to write attestations for docker images produced on the branch,
   or set `DOCKER_ATTEST_SBOM="1"`.
2. To disable SBOM attestations, set `DOCKER_NO_ATTEST_SBOM="1"`.

#### Listing the Software Bill of Materials in a Docker Image

To list all packages in a Docker image, the Docker documentation provides example usage, for example:

```console
$ # list all packages in a Docker container
$ docker buildx imagetools inspect <image> \
    --format "{{ range .SBOM.SPDX.packages }}{{ .name }}@{{ .versionInfo }}{{ println }}{{ end }}"
alpine-baselayout@3.6.5-r0
alpine-baselayout-data@3.6.5-r0
alpine-keys@2.4-r1
apk-tools@2.14.4-r0
busybox@1.36.1-r29
busybox-binsh@1.36.1-r29
ca-certificates-bundle@20240705-r0
libcrypto3@3.3.2-r0
libssl3@3.3.2-r0
musl@1.2.5-r0
musl-utils@1.2.5-r0
scanelf@1.3.7-r2
ssl_client@1.36.1-r29
zlib@1.3.1-r1
```

### Chainguard

If your project uses [Chainguard](https://console.chainguard.dev/overview) images,
the Docker task can automatically log into the GitLab Chainguard account prior to performing the Docker build.

This allows images to be pulled from `cgr.dev` without the need to juggle credentials on a per-project basis.

Currently, due to [OIDC issue #28](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/issues/28), a long-lived token is shared
between all projects under the [`gitlab-com/gl-infra`](https://gitlab.com/groups/gitlab-com/gl-infra/-/settings/ci_cd) group on GitLab.com.

Once the OIDC problem is resolved, these credentials will be removed and replaced with a tokenless OIDC authentication flow.

#### Enabling Chainguard

To automatically log into  `cgr.dev` with a [pull-token](https://edu.chainguard.dev/chainguard/chainguard-registry/authenticating/),
set the variable `CHAINGUARD_VIEWER` to `true` in your docker job, as follows:

```yaml
.container_builds:
  stage: release
  variables:
      CHAINGUARD_VIEWER: true
  ...
```
