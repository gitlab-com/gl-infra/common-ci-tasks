.oidc_id_tokens:
  id_tokens:
    GITLAB_COM_OIDC_TOKEN:
      aud: https://gitlab.com
    OPS_GITLAB_NET_OIDC_TOKEN:
      aud: https://ops.gitlab.net

.oidc_after_script:
  after_script: |
    cat <<-EOD
    ----------------------------------------------------------
    Need help? Documentation on the OIDC CI templates can be found at:
    https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/oidc.md
    EOD

# Note: this is not a full template, just provides a before script
.oidc_setup_script_aws: &oidc_setup_script_aws
  - |
    case $CI_API_V4_URL in
      https://gitlab.com/api/v4)
        GITLAB_OIDC_TOKEN=$GITLAB_COM_OIDC_TOKEN
        GITLAB_OIDC_SITE=https://gitlab.com
        ;;

      https://ops.gitlab.net/api/v4)
        GITLAB_OIDC_TOKEN=$OPS_GITLAB_NET_OIDC_TOKEN
        GITLAB_OIDC_SITE=https://ops.gitlab.net
        ;;

      *)
        echo "Only gitlab.com and ops.gitlab.net are supported for OIDC token exchange with Google Cloud"
        exit 1
        ;;
    esac
  - |
    if [ -z "${GITLAB_OIDC_ROLE_ARN}" ]; then
      echo "GITLAB_OIDC_ROLE_ARN variable must be set when using OIDC."
      echo "Please review documentation at https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/oidc.md"
      exit 1
    fi
  - echo "Performing OIDC exchange using AssumeRoleWithWebIdentity for $GITLAB_OIDC_SITE..."
  - |
    export $(printf "AWS_ACCESS_KEY_ID=%s AWS_SECRET_ACCESS_KEY=%s AWS_SESSION_TOKEN=%s" \
      $(aws sts assume-role-with-web-identity \
        --role-arn "${GITLAB_OIDC_ROLE_ARN}" \
        --role-session-name "GitLabRunner-${CI_PROJECT_ID}-${CI_PIPELINE_ID}-${CI_JOB_ID}" \
        --web-identity-token "${GITLAB_OIDC_TOKEN}" \
        --duration-seconds 3600 \
        --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' \
        --output text \
      ) \
    )
  - aws sts get-caller-identity

.oidc_base_aws:
  extends:
    - .oidc_id_tokens
    - .oidc_after_script
  before_script:
    - *oidc_setup_script_aws
  after_script: |
    cat <<-EOD
    ----------------------------------------------------------
    Need help? Documentation on the oidc_base_aws CI job can be found at:
    https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/oidc.md
    EOD

# See https://cloud.google.com/iam/docs/create-short-lived-credentials-direct
# for documentation on performing a Google Cloud OIDC exchange
.oidc_setup_script_gcp: &oidc_setup_script_gcp
  - |
    if [[ -z ${GCP_OIDC_SERVICE_ACCOUNT_EMAIL} ]]; then
      echo "GCP CI job is misconfigured: required envvar GCP_OIDC_SERVICE_ACCOUNT_EMAIL is not set."
      exit 1
    fi

    case $CI_API_V4_URL in
      https://gitlab.com/api/v4)
        if [[ -z ${GITLAB_COM_OIDC_TOKEN} ]]; then
          echo "GCP CI job is misconfigured: required envvar GITLAB_COM_OIDC_TOKEN is not set."
          exit 1
        fi

        GITLAB_OIDC_TOKEN=$GITLAB_COM_OIDC_TOKEN
        GITLAB_OIDC_SITE=https://gitlab.com
        ;;

      https://ops.gitlab.net/api/v4)
        if [[ -z ${OPS_GITLAB_NET_OIDC_TOKEN} ]]; then
          echo "GCP CI job is misconfigured: required envvar OPS_GITLAB_NET_OIDC_TOKEN is not set."
          exit 1
        fi

        GITLAB_OIDC_TOKEN=$OPS_GITLAB_NET_OIDC_TOKEN
        GITLAB_OIDC_SITE=https://ops.gitlab.net
        ;;

      *)
        echo "Only gitlab.com and ops.gitlab.net are supported for OIDC token exchange with Google Cloud"
        exit 1
        ;;
    esac

    if command -v gcloud > /dev/null 2>&1; then
      # gcloud route with credentials file
      export CLOUDSDK_CONFIG=/var/run/secrets/gcloud
      mkdir -p "${CLOUDSDK_CONFIG}"

      # write the token to a file
      echo "${GITLAB_OIDC_TOKEN}" >"${CLOUDSDK_CONFIG}/id_token"
      chmod 600 "${CLOUDSDK_CONFIG}/id_token"

      gcloud iam workload-identity-pools create-cred-config \
        "${GCP_OIDC_WORKLOAD_ID_POOL_PROVIDER_NAME}" \
        --service-account="${GCP_OIDC_SERVICE_ACCOUNT_EMAIL}" \
        --credential-source-file="${CLOUDSDK_CONFIG}/id_token" \
        --output-file="${CLOUDSDK_CONFIG}/credentials.json"

      gcloud auth login \
        --no-browser \
        --cred-file="${CLOUDSDK_CONFIG}/credentials.json" \
        --update-adc

      if [[ -n "${GCP_PROJECT}" ]]; then
        gcloud config set project "${GCP_PROJECT}"
      fi

      CLOUDSDK_AUTH_ACCESS_TOKEN=$(gcloud auth print-access-token)
      export GOOGLE_CREDENTIALS=${CLOUDSDK_CONFIG}/credentials.json
      export GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_CREDENTIALS
    else
      # Non-gcloud/REST approach...

      OIDC_TOKEN_PAYLOAD="$(cat <<EOF
      {
        "audience": "//iam.googleapis.com/${GCP_OIDC_WORKLOAD_ID_POOL_PROVIDER_NAME}",
        "grantType": "urn:ietf:params:oauth:grant-type:token-exchange",
        "requestedTokenType": "urn:ietf:params:oauth:token-type:access_token",
        "scope": "https://www.googleapis.com/auth/cloud-platform https://www.googleapis.com/auth/userinfo.email",
        "subjectTokenType": "urn:ietf:params:oauth:token-type:jwt",
        "subjectToken": "${GITLAB_OIDC_TOKEN}"
      }
    EOF
    )"

      echo "Performing OIDC exchange using access_token for $GITLAB_OIDC_SITE..."
      FEDERATED_TOKEN=$(
        curl --silent --fail 'https://sts.googleapis.com/v1/token?$fields=access_token&$prettyPrint=false' \
        --header "Accept: application/json" \
        --header "Content-Type: application/json" \
        --data "${OIDC_TOKEN_PAYLOAD}" |
        cut -d\" -f4
      )

      if [[ -z ${FEDERATED_TOKEN} ]]; then
        echo "Failed to obtain federated token from https://sts.googleapis.com/v1/token for GitLab <-> Google Cloud OIDC exchange."
        exit 1
      fi

      echo "Impersonating the service account ${GCP_OIDC_SERVICE_ACCOUNT_EMAIL}..."

      export CLOUDSDK_AUTH_ACCESS_TOKEN=$(
        curl --silent --fail "https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/${GCP_OIDC_SERVICE_ACCOUNT_EMAIL}:generateAccessToken?\$fields=access_token&\$prettyPrint=false" \
        --header "Accept: application/json" \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer ${FEDERATED_TOKEN}" \
        --data '{"scope": ["https://www.googleapis.com/auth/cloud-platform", "https://www.googleapis.com/auth/userinfo.email"]}' \
        | cut -d\" -f4
      )
    fi

    if [[ -z ${CLOUDSDK_AUTH_ACCESS_TOKEN} ]]; then
      echo "Failed to exchange federated token for access token, required for GitLab <-> Google Cloud OIDC exchange."
      exit 1
    fi

    echo "OIDC exchange completed, obtaining userinfo"

    # Check to ensure that the credentials work
    curl --silent --fail https://openidconnect.googleapis.com/v1/userinfo \
      --header "Authorization: Bearer ${CLOUDSDK_AUTH_ACCESS_TOKEN}"

    if command -v terraform > /dev/null 2>&1; then
      # Google Terraform provider: "You can alternatively use the GOOGLE_OAUTH_ACCESS_TOKEN environment variable"
      # https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#access_token
      export GOOGLE_OAUTH_ACCESS_TOKEN="$CLOUDSDK_AUTH_ACCESS_TOKEN"
    fi

# https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/#retrieve-a-temporary-credential
.oidc_base_gcp:
  extends:
    - .oidc_id_tokens
    - .oidc_after_script
  before_script:
    - *oidc_setup_script_gcp

# This is a special use-case for environments that run in both AWS and GCP concurrently
.oidc_base_dual:
  extends:
    - .oidc_id_tokens
    - .oidc_after_script
  before_script:
    - *oidc_setup_script_aws
    - *oidc_setup_script_gcp
