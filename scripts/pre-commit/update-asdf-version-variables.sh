#!/usr/bin/env bash

#
# Run ./scripts/update-asdf-version-variables.sh
#

set -euo pipefail
IFS=$'\n\t'

eval "$(mise activate bash --shims)"

./scripts/update-asdf-version-variables.sh

git diff --quiet .gitlab-ci-asdf-versions.yml || {
  echo ".gitlab-ci-asdf-versions.yml has been updated, please re-add it to your commit"
  exit 2
}
