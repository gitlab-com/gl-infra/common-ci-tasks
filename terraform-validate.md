# [`terraform-validate`](./terraform-validate.yml)

Runs [`terraform validate`](https://www.terraform.io/cli/commands/validate) to ensure that all Terraform files are valid.

1. Ensure that the `GL_ASDF_TERRAFORM_VERSION` version for terraform is configured.
1. Supports the `COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP` variable, for excluding paths from validation.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_TERRAFORM_VERSION: ...
  # Exclude vendor and files directories from validation
  COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP: '^\.(/vendor/|/files/)'

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Ensures that all terraform files are syntactically valid
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/terraform-validate.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: terraform-validate.yml
```

## Determining which Terraform Modules to Run `terraform validate` on...

`terraform validate` will recursively valid all child modules for a given root module.

This means that there is no need to validate the child modules if the root module has been validated.

Unfortunately there is no canonical way to determine if a module is a root module in Terraform or not,
so that the task can selectively run on root modules only.

As a proxy, this task uses the presence of `.terraform.lock.hcl` to signal that a module is a root module.

This has some implications for users of this task:

1. Be sure to check the `.terraform.lock.hcl` files in for your root modules.
1. Only add `.terraform.lock.hcl` for your root modules.
   If a module is not a root module,
   but has a checked in `.terraform.lock.hcl` file,
   it's recommended that you remove it from git.
2. Any module that exists in the repo,
   but does not have a `.terraform.lock.hcl` file
   _and_ is not connected to the root module,
   either directly or indirectly,
   will not be linted.

Running `terraform validate` on root modules only is much more efficient and results in a faster builder,
for relatively little cost.
