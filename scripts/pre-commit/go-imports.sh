#!/usr/bin/env bash

#
# Capture and print stdout, since goimports doesn't use proper exit codes
#

set -euo pipefail
IFS=$'\n\t'

eval "$(mise activate bash --shims)"

if ! command -v goimports &>/dev/null; then
  go install golang.org/x/tools/cmd/goimports@latest
  mise reshim

  if ! command -v goimports &>/dev/null; then
    echo "goimports not available, please install it with "
    echo "go install golang.org/x/tools/cmd/goimports@latest"
    exit 1
  fi
fi

output="$(goimports -l -w "$@")"
echo "$output"
[[ -z $output ]]
