
# [`goreleaser`](./goreleaser.yml)

Including this template will build a binary release of the project using [GoReleaser](https://goreleaser.com/).

It runs `goreleaser build` in the validation stage, and `goreleaser release` on tag pipelines.

It requires a `.goreleaser.yml` file in the root of the project, which looks something like this:

* Default Stages: `validate`, `release`

```
before:
  hooks:
    - go mod tidy
builds:
  - id: "<binaryname>"
    binary: "<binaryname>"
    env:
      - CGO_ENABLED=0
    goos:
      - linux
      - darwin
    goarch:
      - amd64
      - arm64
    ldflags:
      - -s -w -X '{{.ModulePath}}/cmd.version={{.Version}}' -X '{{.ModulePath}}/cmd.commit={{.Commit}}' -X '{{.ModulePath}}/cmd.date={{.Date}}'

archives:
  - replacements:
      darwin: Darwin
      linux: Linux
      amd64: x86_64

dockers:
  - image_templates:
    - "{{ .Env.CI_REGISTRY_IMAGE }}:latest"
    - "{{ .Env.CI_REGISTRY_IMAGE }}:{{ .Tag }}"
    - "{{ .Env.CI_REGISTRY_IMAGE }}:v{{ .Major }}"
    goos: linux
    goarch: amd64

release:
  gitlab:
    owner: gitlab-com/gl-infra # GitLab Group
    name: <projectname> # GitLab Project

checksum:
  name_template: 'checksums.txt'

snapshot:
  name_template: "{{ .Tag }}-next"

changelog:
  sort: asc
  filters:
    exclude:
      - '^docs:'
      - '^test:'

gitlab_urls:
  api: '{{ .Env.CI_SERVER_URL }}'
  download: '{{ .Env.CI_SERVER_URL }}'
```

## Automated Setup

Follow the instructions in **[the Project Setup documentation for Automated Setup](./docs/project-setup.md)** and
ensure that the follow settings are configured on your Project resource:

```terraform

# Enable common-ci-task definitions
common_ci_tasks = {
    enabled          = true
    goreleaser       = true
    ....
}
```

Note that if the `VAULT_SECRETS_PATH` configuration is present, this task will use Vault for resolution of `GITLAB_TOKEN`.

## Manual Setup

For legacy project configurations, the following manual setup is required:

1. Create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with `api`
2. Make it available in your CI environment via the `GITLAB_TOKEN` environment variable through the CI/CD Variables settings.

## `.gitlab-ci.yml` Configuration

```yaml
stages:
  - validate
  - release

variables:
  GL_ASDF_GORELEASER_VERSION: ...

include:
  # build binary release artifacts with goreleaser
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/goreleaser.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: goreleaser.yml
```

## Options

Additional arguments (such as passing environment variables to be consumed by goreleaser) can be set using the `GORELEASER_DOCKER_EXTRA_ARGS` variable.

Additional command line options for Goreleaser can be set using the `GORELEASER_EXTRA_ARGS` argument. These will be appended to the arguments for `goreleaser release`.

## FIPS Mode

If you wish to use `goreleaser` to build your project in a way that is FIPS compliant, you will need to specify the following variables when using the `goreleaser` tasks from this repository:

* `FIPS_MODE`: This instructs the CI pipeline to use the gorelease job specifically for creating FIPS compliant releases
* `GL_ASDF_GORELEASER_VERSION`: The version of goreleaser to use, minimum 1.13.1
* `GL_ASDF_GOLANG_VERSION`: The version of golang to use to compile, minimum 1.19

An example

```yaml
stages:
  - validate
  - release

variables:
  GL_ASDF_GORELEASER_VERSION: ...
  GL_ASDF_GOLANG_VERSION: ...
  FIPS_MODE: 1

  # GORELEASER_BUILD_EXTRA_ARGS are additional targets for goreleaser build, usually useful when only certain build ids
  # be targetted with `--id` argument.
  # This argument is not used during the release process, only during the `goreleaser_build` job in
  # non-release pipelines.
  GORELEASER_BUILD_EXTRA_ARGS: ...

include:
  # build binary release artifacts with goreleaser
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/goreleaser.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: goreleaser.yml
```

## Verifying FIPS mode artifacts

When running Goreleaser in FIPS mode, the Goreleaser template job will verify that the binary has been correctly compiled.

This is done using the technique suggested in the GitLab Handbook: <https://docs.gitlab.com/ee/development/fips_compliance.html#go>, using `go tool nm`.

### Requirements for Verification

1. The FIPS binary must have the suffix `-fips` or `_fips`. This can be configured via Goreleaser's `builds[].binary` configuration.
2. The FIPS binary must not be stripped. Make sure that the `builds[].ldflags` option does not include `-w -s`.
3. The FIPS binary must be compiled with `GCO_ENABLED=1` and `GOEXPERIMENT=boringcrypto` in the `builds[].env` configuration.

Note that Go programs compiled for FIPS compliances are substantially slower than programs which use Go's native crypto.
For this reason, it's recommended to configure both FIPS and non-FIPS release artifacts in Goreleaser,
instead of relying on the FIPS artifact for both.

### I'm seeing `failed to pull image` errors...

In order to generate FIPS compliant images, the Goreleaser task uses [a combination container image for the Goreleaser AND Golang-FIPS version](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/-/blob/main/.gitlab-ci.yml).
These images use a combination of the Golang version (`GL_ASDF_GOLANG_VERSION`) and Goreleaser version (`GL_ASDF_GORELEASER_VERSION`) for performing cross-compilation. Not all combinations are supported.
Not that [Golang-FIPS](https://github.com/golang-fips/go) tends to lag behind the latest version of Golang by a few patch versions.


If you see errors such as `ERROR: Job failed: failed to pull image "registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/goreleaser-golang-fips:v1.20.1-v1.14.1"`
when running the validation step, this probably means there is no combination of Golang-FIPS version/Goreleaser version.

Review the tags for the `registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/goreleaser-golang-fips` image in the Container Registry to find a valid combination: <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/6799192>,
or send a merge-request to add a new combination.

## Out-of-Memory Issues

If you're experiencing OOM kills during the Goreleaser build, consider using a bigger runner by updating the `tags` on the `goreleaser` job. It may also help to reduce paralellism in the Goreleaser build tool.

This can be done as follows:

```yaml
goreleaser:
  tags: [saas-linux-medium-amd64]
  variables:
    GORELEASER_EXTRA_ARGS: "-p 1"
```

## Use Sigstore for keyless signing and verification

The [Sigstore](https://www.sigstore.dev/) project provides a CLI called Cosign which can be used for keyless signing of container images built with GitLab CI/CD. Keyless signing has many advantages, including eliminating the need to manage, safeguard, and rotate a private key.

### Signing Docker images

In order to sign Docker images created with Goreleaser, follow the documentation at <https://goreleaser.com/customization/docker_sign/>.

The following example `.goreleaser.yml` shows how to configure `docker_signs` for keyless signing of Docker images.

```yaml
# .goreleaser.yml

# https://goreleaser.com/customization/docker_sign/
docker_signs:
  - id: keyless_cosign
    cmd: cosign
    args:
      - sign
      - "${artifact}"
    artifacts: all
    ids:
      - docker
    output: true
```

### Signing Binary Artifacts

In order to sign binary artifacts created with Goreleaser, follow the documentation at <https://goreleaser.com/customization/binary_sign/>.

The following example `.goreleaser.yml` shows how to configure `binary_signs` for keyless signing of artifacts.

```yaml
# .goreleaser.yml
# https://goreleaser.com/customization/binary_sign/
binary_signs:
  - cmd: cosign
    args:
      - "sign-blob"
      - "${artifact}"
    output: true
```

## Performing a Full-Mock Release

While the normal Goreleaser build action is helpful in validating that a Go program compiles,
sometimes it's useful to perform a full mock release.
This will full test the Goreleaser release process, including SBOM generation, code signing, docker build, etc.

This can be useful when refactoring the Goreleaser configuration.

In order to use this feature, either add the ~goreleaser-mock-release label to the Merge Request, or
set the `GORELEASER_FULL_MOCK_RELEASE` variable to 1:

```yaml
variables:
  GORELEASER_FULL_MOCK_RELEASE: 1
```
