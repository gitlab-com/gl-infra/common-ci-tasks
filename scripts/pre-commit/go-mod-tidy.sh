#!/usr/bin/env bash

#
# Run go mod tidy for pre-commit repositories
#

set -euo pipefail
IFS=$'\n\t'

eval "$(mise activate bash --shims)"

go mod tidy -v || {
  exit 2
}

git diff --quiet go.* || {
  echo "go.mod or go.sum differs, please re-add it to your commit"
  exit 3
}
