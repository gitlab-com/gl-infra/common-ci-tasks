#!/usr/bin/env bash

#
# Run terra-transformer validate on root modules
#

set -euo pipefail
IFS=$'\n\t'

(mise plugin list | grep -q terra-transformer) || {
  mise plugins install --yes terra-transformer https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra.git
}

mise install terra-transformer

eval "$(mise activate bash --shims)"

main() {
  local failed=false

  while read -r lockfile_path; do
    local terraform_module_path
    terraform_module_path=$(dirname "$lockfile_path")

    echo "Validating root module ${terraform_module_path}"
    terra-transformer validate --chdir="${terraform_module_path}" "$@" || {
      failed=true
    }
  done <<<"$(git ls-files | grep '.terraform.lock.hcl$')"

  if [[ $failed == true ]]; then
    exit 1
  fi

}

main "$@"
