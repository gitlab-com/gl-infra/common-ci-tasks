# [`autolabels`](./autolabels.yml)

The **autolabels** job adds [Work Type Classification](https://handbook.gitlab.com/handbook/product/groups/product-analysis/engineering/metrics/#work-type-classification) labels to GitLab merge requests.
See https://gitlab.com/gitlab-com/gl-infra/autolabels for more details on what *autolabels* does.

## Setup

* Using [infra-mgmt](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt) **(Recommended)**

  Setting `common_ci_tasks.enabled = true` will enable *autolabels* by default.
* Manual setup
  * Create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) (PrAT) with the `api` and `read_repository` scopes and the `Developer` role.

  * Store the PrAT in Vault under the `ci/access_tokens/${VAULT_SECRETS_PATH}/autolabels` path.
  Please note that merge requests need access to this token.
  This is usually achieved by adding the Vault path to the read-only policy.
  * **Alternative:** Create a CI variable called `AUTOLABELS_TOKEN` containing the PrAT.
  Please note that merge requests need access to this token.
  This typically means that you must omit the `Protected` flag, since merge request branches are typically not protected.
  The token has the `Developer` role, which is also the role required to create merge requests, meaning developers cannot escalate their privileges by extracting the token.
* Import `autolabels.yml` into your project:

  ```yaml
  include:
    - project: "gitlab-com/gl-infra/common-ci-tasks"
      ref: v2.63.0  # renovate:managed
      file: "autolabels.yml"
  ```
