# [`go-mod-tidy`](./go-mod-tidy.yml)

Ensures that [`go mod tidy`](https://go.dev/ref/mod) is up-to-date and `go.mod` and `go.sum` are tidy.

1. Ensure that the `GL_ASDF_GOLANG_VERSION` version for Go is configured.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_GOLANG_VERSION: ...

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Perform `go mod tidy` and ensure that go.mod and go.sum are tidy.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/go-mod-tidy.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: go-mod-tidy.yml
```
## A note on compatibility

When Go 1.16 or Go 1.17 is used, `go mod tidy` is configured to use the `-compat=1.17` flag, setting the minimum version of `go` that can be used for consuming modules is `1.17`. With more recent versions of Go, no `-compat=` flag is set.
