# Project Setup

There are two ways of setting up projects: the automated way and the manual way. The manual way is legacy only.

## Automated Project Setup

1. Declare a new project in the **[Infra-Mgmt project](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/tree/main/environments/gitlab-com)**.
   1. Infra-Mgmt configuration will ensure that Vault configuration is applied to the project.
   2. The **[Project module](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/gitlab/project)** contains
      the documentation on configuring a new project appropriately.
1. Add the `common-ci-tasks` input variable to the Project resource declaration, as follows:

    ```terraform

    module "project_gitlab-dedicated-org-tenctl" {
        source  = "ops.gitlab.net/gitlab-com/project/gitlab"
        version = "x.x.x"

        ...

        # Enable common-ci-task definitions
        common_ci_tasks = {
            enabled          = true
            goreleaser       = true  # Enable for Golang projects that use Goreleaser, see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/goreleaser.md
            mirroring        = true
            renovate_bot     = true
            semantic_release = true
            danger           = false # Enable for projects using Danger
        }
    }
    ```
1. Merge the change and await Terraform provisioning of the project.
1. Optional, but recommended. Follow the **[Project Templates documentation](./project-templates.md) to setup the new project
   using a [copier template](https://copier.readthedocs.io/).
   Copier has the advantage of allowing existing project templates to be progressively updated as new changes are made
   to the template.

## Manual Project Setup

Each template has it's own requirements for manual setup. Follow the template documentation for details.

You'll also need to maintain and refresh tokens manually, so this approach will require a great deal more overhead in the long run.
