# [`shfmt`](./shfmt.yml)

Validates that shell-scripts use a canonical formatting.

1. Ensure that the `GL_ASDF_SHFMT_VERSION` version for Go is configured.
1. Supports the `COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP` variable, for excluding paths from validation.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_SHFMT_VERSION: ...
  # Exclude vendor and files directories from validation
  COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP: '^\.(/vendor/|/files/)'

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Ensure that all shell-scripts are formatted according to a
  # standard canonical format
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: shfmt.yml
```
