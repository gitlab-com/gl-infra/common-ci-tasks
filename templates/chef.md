# [`templates/chef.yml`](./templates/chef.yml)

This template should be used by Chef cookbook projects

The common tasks performed by this template are:

1. Lint the cookbook with `cookstyle`
1. Ensure that test suites included in kitchen.yml have also been added to `.gitlab-ci.yml`
1. Run rspec tests
1. Run kitchen tests
1. Publish the cookbook to the Chef server if running against the master branch.

## Required stages

```yaml
stages:
  - prepare
  - lint
  - unit
  - integration
  - publish
```

You can override these using input variables, such as:
```yaml
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: master
    file: templates/chef.yml
    inputs:
      lint_stage: test
```

## Project requirements

At minimum, the following Ruby gems must be included for common testing tasks.

- [chef](https://rubygems.org/gems/chef)
- [chefspec](https://rubygems.org/gems/chefspec)
- [cookstyle](https://rubygems.org/gems/cookstyle)
- [kitchen-google](https://rubygems.org/gems/kitchen-google)
- [kitchen-inspec](https://rubygems.org/gems/kitchen-inspec)
- [rspec](https://rubygems.org/gems/rspec)
- [test-kitchen](https://rubygems.org/gems/test-kitchen)

This has been tested on Ruby 2.6+

## Kitchen testing

This template provides a basic job for executing Kitchen tests during the integration stage.

However, it is likely that you will want to override this in the event you have multiple Kitchen test suites.

An example overridden job that you may have in your repo's local `.gitlab-ci.yml` may look like:

```yaml
kitchen:
  stage: integration
  retry: 2
  artifacts:
    expire_in: '2d'
    when: always
    paths:
      - .kitchen/
    reports:
      junit: .kitchen/*_inspec.xml
  cache:
    paths:
      - \$BUNDLE_PATH
  script:
    - SSH_KEY="$HOME/.ssh/id_ed25519" bundle exec kitchen test --destroy=always $KITCHEN_SUITE
  parallel:
    matrix:
      - KITCHEN_SUITE:
        # generate with: make update-ci
        # SUITES_BEGIN
        # SUITES_END
```

Note the comments in the KITCHEN_SUITE parallel matrix. These are to be used in conjuncture with a `update-ci` task defined in a common [`Makefile`](https://gitlab.com/gitlab-cookbooks/gitlab-server/-/blob/0ca860808ed4c0ce3e09ec8572563c161eceeb05/Makefile#L63-73). If these comments exist, and multiple kitchen test suites are defined in `kitchen.yml`, this `make` command will automatically add all suites to `.gitlab-ci.yml` when run.

If a cookbook doesn't have kitchen tests, you can remove them from execution by changing the execution stage like:

```yaml
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: master
    file: templates/chef.yml
    inputs:
      integration_stage: skip
```
