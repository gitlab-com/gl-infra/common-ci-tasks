#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

main() {
  mise install shfmt
  eval "$(mise activate bash --shims)"

  echo "Reapplying shfmt formatting for updated version..."
  (shfmt --find . |
    grep -vE "${COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP:-__ignored__}" |
    xargs shfmt -w -l -s -i 2 -d) || true # Don't treat a failed exit code as a script failure
}

main
