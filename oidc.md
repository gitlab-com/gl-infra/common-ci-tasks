# OIDC Extensible Tasks

## [`.oidc_base`](./oidc.yml)

This is not a full template, but allows users to easily define their own [OIDC](https://docs.gitlab.com/ee/ci/cloud_services/#oidc-authorization-with-your-cloud-provider) setup scripts for establishing trust relationships with cloud providers.

### Using AWS OIDC

Full instructions on setting up AWS OIDC are available in the [GitLab Docs](https://docs.gitlab.com/ee/ci/cloud_services/aws/index.html). Setup an appropriate OIDC provider for `gitlab.com` or `ops.gitlab.net`.

Then extend from the `.oidc_base_aws` job as follows, ensuring that the `GITLAB_OIDC_ROLE_ARN` variable is configured with the IAM Role that the CI job would like to assume.

```yaml
include:
  # Includes a base template for oidc authentication
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/oidc.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'oidc.yml'

deploy_to_cloud_provider:
  variables:
    GITLAB_OIDC_ROLE_ARN: "arn:aws:iam::123456789012:role/ci/GitLabCI"
  extends:
    - .oidc_base_aws
  script:
    # Thanks to OIDC configuration, AWS is authenticated and
    # can be used without further authentication steps.
    - terraform apply -auto-approve
```

### Using GCP OIDC

Full instructions on setting up GCP OIDC are available in the [GitLab Docs](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/). Setup an appropriate OIDC provider for `gitlab.com` or `ops.gitlab.net`.

Three variables are required for the job to work:

1. `GCP_OIDC_WORKLOAD_ID_POOL_PROVIDER_NAME`: the name of the GCP IAM Workload Identity Pool.
2. `GCP_OIDC_SERVICE_ACCOUNT_EMAIL`: the email address of the service account to impersonate.
2. `GCP_PROJECT`: The ID of the GCP project.

Then extend from the `.oidc_base_gcp` job as follows, passing in the three required variables:

```yaml
include:
  # Includes a base template for running kaniko easily
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/kaniko.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'oidc.yml'

oidc_authenticated_job:
  image:
    name: google/cloud-sdk:alpine
    entrypoint: [""]
  variables:
    # NB NB NB: change these values to appropriate values for your use-case
    # Read on if you're looking to details to obtain these values.
    GCP_OIDC_WORKLOAD_ID_POOL_PROVIDER_NAME: projects/253796970094/locations/global/workloadIdentityPools/gitlab-pool-oidc-46438647/providers/gitlab-jwt-46438647
    GCP_OIDC_SERVICE_ACCOUNT_EMAIL: gitlab-oidc@andrew-06c9c6d0.iam.gserviceaccount.com
    GCP_PROJECT: andrew-06c9c6d0
  extends:
    - .oidc_base_gcp
  script:
    # Thanks to OIDC configuration, GCP is authenticated and
    # can be used without further authentication steps.
    - gcloud iam service-accounts list
```

### Notes

1. The `curl` binary is required
1. Other than the dependency on `curl`, GCP OIDC exchange can take place in any container image -- the `gcloud` binary is not mandatory.

### Configuring the OIDC Provider for GCP

The GCP OIDC Terraform module, from GitLab's Security Operations Team, is recommended: see <https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security-public/oidc-modules/> for more details.

The following Terraform can be used to create an OIDC trust relationship between a GitLab project and a Google Cloud Project.

```terraform
locals {
  google_project_id    = "<SET_GCP_PROJECT_ID_HERE>" # note: this is a string
  gitlab_project_id    = <SET_GITLAB_PROJECT_HERE>   # note: this is a number
  service_account_role = "roles/editor"              # choose an appropriate role
}

terraform {
  required_version = "~> 1.4"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.55.0"
    }
  }
}

resource "google_service_account" "oidc" {
  account_id   = "gitlab-oidc"
  description  = "Service Account for GitLab OIDC"
  disabled     = "false"
  display_name = "GitLab-OIDC"
  project      = local.google_project_id
}

resource "google_project_iam_member" "oidc" {
  member  = "serviceAccount:${google_service_account.oidc.email}"
  project = local.google_project_id
  role    = local.service_account_role
}

module "gl_oidc" {
  # Documentation:
  # https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security-public/oidc-modules/-/tree/main/terraform-modules/gcp-oidc
  # https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/
  source = "gitlab.com/gitlab-com/gcp-oidc/google"
  version = "1.2.2"

  google_project_id = local.google_project_id
  gitlab_project_id = local.gitlab_project_id

  allowed_audiences = ["https://gitlab.com"]

  oidc_service_account = {
    "sa" = {
      sa_email  = google_service_account.oidc.email
      attribute = "attribute.project_id/${local.gitlab_project_id}"
    }
  }
}

output "workload_identity_pool_provider_name" {
  value = module.gl_oidc.workload_identity_pool_provider_name
}

output "service_account_email" {
  value = google_service_account.oidc.email
}
```

Once the project is applied:

1. Use the `workload_identity_pool_provider_name` output as the value for the `GCP_OIDC_WORKLOAD_ID_POOL_PROVIDER_NAME` variable.
2. Use the `service_account_email` output as the value for the `GCP_OIDC_SERVICE_ACCOUNT_EMAIL` variable.

### Dual GCP/AWS OIDC authentication

This is a special use-case for situations where a CI/CD job needs to perform OIDC authentication exchanges against both GCP and AWS targets.

This combines the AWS and GCP scripts in one, please review the requirements for both (above).

Extend from the `.oidc_base_gcp` job as follows, passing in the three required variables:

```yaml
include:
  # Includes a base template for running kaniko easily
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/kaniko.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: 'oidc.yml'

dual_oidc_authenticated_job:
  image:
    name: google/cloud-sdk:alpine
    entrypoint: [""]
  variables:
    # NB NB NB: change these values to appropriate values for your use-case
    # Read on if you're looking to details to obtain these values.
    GCP_OIDC_WORKLOAD_ID_POOL_PROVIDER_NAME: projects/253796970094/locations/global/workloadIdentityPools/gitlab-pool-oidc-46438647/providers/gitlab-jwt-46438647
    GCP_OIDC_SERVICE_ACCOUNT_EMAIL: gitlab-oidc@andrew-06c9c6d0.iam.gserviceaccount.com
    GCP_PROJECT: andrew-06c9c6d0
    GITLAB_OIDC_ROLE_ARN: "arn:aws:iam::123456789012:role/ci/GitLabCI"
  extends:
    - .oidc_base_dual
  script:
    # Thanks to OIDC configuration, GCP is authenticated and
    # can be used without further authentication steps.
    - gcloud iam service-accounts list
```
