# Infrastructure Project Mirroring Template

## [`.mirroring`](./mirroring.yml)

Many Infrastructure projects are hosted on one GitLab instance (usually `gitlab.com`), but run pipelines from another (usually `ops.gitlab.net`).

This template assists in setting up the [Woodhouse `gitlab notify-mirrored-mr`](https://gitlab.com/gitlab-com/gl-infra/woodhouse#gitlab-notify-mirrored-mr-subcommand) notifications and optionally (if a token is supplied) blocks on [Woodhouse gitlab follow-remote-pipeline`](https://gitlab.com/gitlab-com/gl-infra/woodhouse#gitlab-follow-remote-pipeline-subcommand) remote pipelines.

1. If `gitlab.com` is the source, and `ops.gitlab.net` is the mirror
    1. Configure a variable, `GITLAB_INFRA_MIRROR` with a value of `ops.gitlab.net`.
    1. Optionally include a secret on `gitlab.com` called `GITLAB_OPS_API_TOKEN` containing a API token to check on the status of the mirrored CI/CD pipeline.

```yaml
stages:
  - validate

variables:
  # This project is hosted primarily on gitlab.com,
  # with a mirror on ops.gitlab.net
  GITLAB_INFRA_MIRROR: "ops.gitlab.net"

include:
  # Setup Woodhouse notifications in merge requests for mirrored remote pipelines.
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: mirroring.yml
```
