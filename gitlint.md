# [`gitlint`](./gitlint.yml)

Runs [`gitlint`](https://jorisroovers.com/gitlint/) on the latest commit message in a merge-request.

Setup process:

1. Ensure that a [`.gitlint`](https://jorisroovers.com/gitlint/configuration/) file exists in the root of the project.

```yaml
stages:
  - validate

include:
  # Runs gitlint on all terraform module directories
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/gitlint.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.63.0  # renovate:managed
    file: gitlint.yml
```
